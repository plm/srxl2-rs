use srxl2::parser::*;
use srxl2::protocol::*;
use srxl2::serializer::*;

use std::env;
use std::fmt::Debug;
use std::thread;
use std::time::Duration;

use serial::prelude::*;

const TELEMETRY_DATA: [u8; TELEMETRY_PACKET_LENGTH] = [
    0x20, 0x00, 0x07, 0xF9, 0x07, 0xD0, 0x03, 0x91, 0x00, 0x05, 0xFF, 0xFF, 0x00, 0x50, 0x02, 0x00,
];

#[derive(Default)]
struct PacketPrinter {
    count_in: u32,
    count_out: u32,
}

impl PacketPrinter {
    fn print_in(&mut self, packet: &impl Debug) {
        self.count_in += 1;
        println!(">> #{} {:?}", self.count_in, packet);
    }

    fn print_out(&mut self, packet: &impl Debug) {
        self.count_out += 1;
        println!("<< #{} {:?}", self.count_out, packet);
    }
}

fn main() {
    for arg in env::args_os().skip(1) {
        let mut port = serial::open(&arg).unwrap();
        interact(&mut port);
    }
}

fn interact<T: SerialPort>(port: &mut T) -> ! {
    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud115200)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    })
    .unwrap();

    port.set_timeout(Duration::from_millis(50)).unwrap();

    let mut out_packet: Option<PacketPayload> = None;
    let mut out_buffer: WriteBuffer = WriteBuffer::new();

    let my_id = DeviceType::ESC.with_id(0x9).unwrap();
    let my_priority = Priority::try_from(10).unwrap();
    let my_uid = UniqueID::from(0x12345678);

    let baud_rate = 115_200;

    let mut in_buffer = ReadBuffer::new();

    let mut sent_handshake = false;

    let mut telemetry_destination = TelemetryDestination::Disabled;

    let mut printer = PacketPrinter::default();

    thread::sleep(Duration::from_millis(50));

    loop {
        if let Some(payload) = out_packet.take() {
            printer.print_out(&payload);
            thread::sleep(Duration::from_micros(2 * 10 * 1_000_000 / baud_rate));
            let _ = port.write_all(out_buffer.write(&payload));
        }
        if sent_handshake {
            if in_buffer.fill(|buf| port.read(buf)).is_ok() {
                if let Ok(payload) = in_buffer.read() {
                    printer.print_in(&payload);
                    match payload {
                        PacketPayload::Handshake(Handshake {
                            source,
                            destination,
                            ..
                        }) => match destination {
                            DeviceID::Typed(id) if id == my_id => {
                                let _ = out_packet.insert(
                                    Handshake::new(
                                        my_id,
                                        source.into(),
                                        my_priority,
                                        BaudSupported::Only115200,
                                        DeviceOptions::EnableTelemetryOverRF,
                                        my_uid,
                                    )
                                    .into(),
                                );
                            }
                            DeviceID::Broadcast => {
                                telemetry_destination = TelemetryDestination::Receiver(source)
                            }
                            _ => {}
                        },
                        PacketPayload::ControlData(control_data) => {
                            if control_data.reply_id == my_id.into() {
                                let _ = out_packet.insert(
                                    TelemetrySensorData::new(
                                        telemetry_destination,
                                        TelemetryPacket::new(TELEMETRY_DATA),
                                    )
                                    .into(),
                                );
                            }
                        }
                        _ => {}
                    };
                }
            }
        } else {
            thread::sleep(Duration::from_millis(50));
            let _ = out_packet.insert(
                Handshake::new(
                    my_id,
                    DeviceID::NotSpecified,
                    my_priority,
                    BaudSupported::Only115200,
                    DeviceOptions::EnableTelemetryOverRF,
                    my_uid,
                )
                .into(),
            );
            sent_handshake = true;
        }
    }
}
