#![cfg_attr(not(feature = "std"), no_std)]

pub mod parser;
pub mod protocol;
pub mod serializer;

pub(crate) mod sealed {
    pub trait Sealed {}
}

#[cfg(test)]
mod tests {

    #[test]
    #[should_panic]
    fn canary() {
        panic!();
    }
}
