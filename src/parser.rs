use super::protocol::*;
use core::cmp;
use core::fmt;
use core::mem;
use core::ops::RangeInclusive;
use crc::{Crc, CRC_16_XMODEM};
use heapless::Vec;
use winnow::{
    binary::{be_u16, le_i8, le_u16, le_u32, le_u64, le_u8},
    combinator::{fill, preceded, repeat, rest, terminated, trace},
    error::{ContextError, ErrMode},
    stream::{AsBytes, Stream, StreamIsPartial},
    token::{one_of, take},
    PResult, Parser, Partial,
};

type Input<'i> = &'i [u8];
type Output<O> = PResult<O>;

fn srxl2<I>(i: &mut I) -> Output<u8>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("srxl2", one_of(Protocol::SRXL2)).parse_next(i)
}

const PACKET_LENGTHS: RangeInclusive<usize> = MIN_PACKET_LENGTH..=MAX_PACKET_LENGTH;

fn packet_length<I>(i: &mut I) -> Output<usize>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "packet_length",
        le_u8
            .map(usize::from)
            .verify(|l| PACKET_LENGTHS.contains(l)),
    )
    .parse_next(i)
}

fn priority<I>(i: &mut I) -> Output<Priority>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("priority", le_u8.try_map(Priority::try_from)).parse_next(i)
}

fn unique_id<I>(i: &mut I) -> Output<UniqueID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("unique_id", le_u32.map(UniqueID::from)).parse_next(i)
}

fn typed_device_id<I>(i: &mut I) -> Output<TypedDeviceID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("typed_device_id", le_u8.try_map(TypedDeviceID::try_from)).parse_next(i)
}

fn device_id<I>(i: &mut I) -> Output<DeviceID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("device_id", le_u8.try_map(DeviceID::try_from)).parse_next(i)
}

fn packet_type<I>(i: &mut I) -> Output<PacketType>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("packet_type", move |i: &mut I| {
        le_u8.try_map(PacketType::try_from).parse_next(i)
    })
    .parse_next(i)
}

fn header<I>(i: &mut I) -> Output<(PacketType, usize)>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("header", move |i: &mut I| {
        preceded(srxl2, (packet_type, packet_length)).parse_next(i)
    })
    .parse_next(i)
}

fn packet<'i, I>(i: &mut I) -> Output<(usize, PacketPayload)>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
    I: 'i,
{
    trace("packet", move |i: &mut I| {
        let checkpoint = i.checkpoint();
        let (ptype, len) = header.parse_next(i)?;
        let start = i.offset_from(&checkpoint);
        i.reset(&checkpoint);
        let data = take(len - mem::size_of::<u16>()).parse_next(i)?;
        let data = data.as_bytes();
        let _ = be_u16
            .verify(|&v| v == Crc::<u16>::new(&CRC_16_XMODEM).checksum(data))
            .parse_next(i)?;
        (match ptype {
            PacketType::Handshake => handshake_payload,
            PacketType::BindInfo => bind_info_payload,
            PacketType::ParameterConfiguration => parameter_configuration_payload,
            PacketType::SignalQuality => signal_quality_payload,
            PacketType::TelemetrySensorData => telemetry_sensor_data_payload,
            PacketType::ControlData => control_data_payload,
        })
        .complete_err()
        .parse_next(&mut &data[start..])
        .map(|p| (len, p))
    })
    .parse_next(i)
}

fn baud_supported<I>(i: &mut I) -> Output<BaudSupported>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("baud_supported", le_u8.try_map(BaudSupported::try_from)).parse_next(i)
}

fn device_options<I>(i: &mut I) -> Output<DeviceOptions>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("device_options", le_u8.try_map(DeviceOptions::try_from)).parse_next(i)
}

fn handshake<I>(i: &mut I) -> Output<Handshake>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("handshake", move |i: &mut I| {
        let source = typed_device_id(i)?;
        let destination = device_id(i)?;
        let priority = priority(i)?;
        let baud_supported = baud_supported(i)?;
        let device_options = device_options(i)?;
        let uid = unique_id(i)?;
        Ok(Handshake::new(
            source,
            destination,
            priority,
            baud_supported,
            device_options,
            uid,
        ))
    })
    .parse_next(i)
}

fn handshake_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("handshake_payload", handshake.map(PacketPayload::Handshake)).parse_next(i)
}

fn bind_request<I>(i: &mut I) -> Output<BindRequest>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("bind_request", le_u8.try_map(BindRequest::try_from)).parse_next(i)
}

fn bind_status<I>(i: &mut I) -> Output<BindStatus>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("bind_status", le_u8.try_map(BindStatus::try_from)).parse_next(i)
}

fn bind_options<I>(i: &mut I) -> Output<BindOptions>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("bind_options", le_u8.try_map(BindOptions::try_from)).parse_next(i)
}

fn guid<I>(i: &mut I) -> Output<GUID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("guid", le_u64.map(GUID::from)).parse_next(i)
}

fn parameter_configuration_request<I>(i: &mut I) -> Output<ParameterConfigurationRequest>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "parameter_configuration_request",
        le_u8.try_map(ParameterConfigurationRequest::try_from),
    )
    .parse_next(i)
}

fn param_id<I>(i: &mut I) -> Output<ParamID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("param_id", le_u32.map(ParamID::from)).parse_next(i)
}

fn param_value<I>(i: &mut I) -> Output<ParamValue>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("param_value", le_u32.map(ParamValue::from)).parse_next(i)
}

fn reply_id<I>(i: &mut I) -> Output<ReplyID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("reply_id", le_u8.try_map(ReplyID::try_from)).parse_next(i)
}

fn rssi<I>(i: &mut I) -> Output<RSSI>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("rssi", le_i8.map(RSSI::from)).parse_next(i)
}

fn frame_losses<I>(i: &mut I) -> Output<FrameLosses>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("frame_losses", le_u16.map(FrameLosses::from)).parse_next(i)
}

fn channel_value<I>(i: &mut I) -> Output<ChannelValue>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("channel_value", le_u16.try_map(ChannelValue::try_from)).parse_next(i)
}

fn channel_values<I>(i: &mut I) -> Output<ChannelValues>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "channel_values",
        le_u32.flat_map(|mask| {
            repeat(mask.count_ones() as usize, channel_value)
                .fold(Vec::new, |mut values, v| {
                    let _ = values.push(v);
                    values
                })
                .try_map(move |values| ChannelValues::try_from((mask, values)))
        }),
    )
    .parse_next(i)
}

fn rf_link_state<I>(i: &mut I) -> Output<RFLinkState>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("rf_link_state", channel_values.map(RFLinkState::from)).parse_next(i)
}

fn channel_data<I>(i: &mut I) -> Output<ChannelData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("channel_data", move |i: &mut I| {
        let rssi = rssi(i)?;
        let frame_losses = frame_losses(i)?;
        let link_state = rf_link_state(i)?;
        Ok(ChannelData::new(rssi, frame_losses, link_state))
    })
    .parse_next(i)
}

fn channel_data_payload<I>(i: &mut I) -> Output<ControlDataPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "channel_data_payload",
        channel_data.map(ControlDataPayload::ChannelData),
    )
    .parse_next(i)
}

fn holds<I>(i: &mut I) -> Output<Holds>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("holds", le_u16.map(Holds::from)).parse_next(i)
}

fn control_data_command<I>(i: &mut I) -> Output<ControlDataCommand>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "control_data_command",
        le_u8.try_map(ControlDataCommand::try_from),
    )
    .parse_next(i)
}

fn failsafe_channel_data<I>(i: &mut I) -> Output<FailsafeChannelData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("failsafe_channel_data", move |i: &mut I| {
        let rssi = rssi(i)?;
        let holds = holds(i)?;
        let values = channel_values(i)?;
        Ok(FailsafeChannelData::new(rssi, holds, values))
    })
    .parse_next(i)
}

fn failsafe_channel_data_payload<I>(i: &mut I) -> Output<ControlDataPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "failsafe_channel_data_payload",
        failsafe_channel_data.map(ControlDataPayload::FailsafeChannelData),
    )
    .parse_next(i)
}

fn reserved_control_data_payload<I>(i: &mut I) -> Output<ControlDataPayload>
where
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("reserved_control_data_payload", move |i: &mut I| {
        let data = rest
            .verify_map(|r: <I as Stream>::Slice| {
                let data = r.as_bytes();
                if data.len() > CONTROL_DATA_PAYLOAD_LENGTH {
                    None
                } else {
                    Vec::from_slice(data).ok()
                }
            })
            .parse_next(i)?;
        Ok(ControlDataPayload::Reserved(data))
    })
    .parse_next(i)
}

fn control_data<I>(i: &mut I) -> Output<ControlData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("control_data", move |i: &mut I| {
        let command = control_data_command(i)?;
        let recipient = reply_id(i)?;
        let payload = match command {
            ControlDataCommand::ChannelData => channel_data_payload,
            ControlDataCommand::FailsafeChannelData => failsafe_channel_data_payload,
            ControlDataCommand::VTXData => vtx_data_payload,
            ControlDataCommand::Reserved => reserved_control_data_payload,
        }
        .parse_next(i)?;
        Ok(ControlData::new(recipient, payload))
    })
    .parse_next(i)
}

fn control_data_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace(
        "control_data_payload",
        control_data.map(PacketPayload::ControlData),
    )
    .parse_next(i)
}

fn signal_quality_request<I>(i: &mut I) -> Output<SignalQualityRequest>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "signal_quality_request",
        le_u8.try_map(SignalQualityRequest::try_from),
    )
    .parse_next(i)
}

fn telemetry_packet<I>(i: &mut I) -> Output<TelemetryPacket>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("telemetry_packet", move |i: &mut I| {
        let mut bytes = [0x0; TELEMETRY_PACKET_LENGTH];
        fill(le_u8, &mut bytes).parse_next(i)?;
        Ok(TelemetryPacket::new(bytes))
    })
    .parse_next(i)
}

fn vtx_band<I>(i: &mut I) -> Output<VTXBand>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_band", le_u8.try_map(VTXBand::try_from)).parse_next(i)
}

fn vtx_channel<I>(i: &mut I) -> Output<VTXChannel>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_channel", le_u8.try_map(VTXChannel::try_from)).parse_next(i)
}

fn vtx_mode<I>(i: &mut I) -> Output<VTXMode>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_mode", le_u8.try_map(VTXMode::try_from)).parse_next(i)
}

fn vtx_region<I>(i: &mut I) -> Output<VTXRegion>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_region", le_u8.try_map(VTXRegion::try_from)).parse_next(i)
}

fn vtx_power<I>(i: &mut I) -> Output<VTXPower>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_power", (le_u8, le_u16).try_map(VTXPower::try_from)).parse_next(i)
}

fn vtx_data<I>(i: &mut I) -> Output<VTXData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("vtx_data", move |i: &mut I| {
        let band = vtx_band(i)?;
        let channel = vtx_channel(i)?;
        let mode = vtx_mode(i)?;
        let power = vtx_power(i)?;
        let region = vtx_region(i)?;
        Ok(VTXData::new(band, channel, mode, power, region))
    })
    .parse_next(i)
}

fn vtx_data_payload<I>(i: &mut I) -> Output<ControlDataPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "vtx_data_payload",
        vtx_data.map(ControlDataPayload::VTXData),
    )
    .parse_next(i)
}

fn bind_info<I>(i: &mut I) -> Output<BindInfo>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("bind_info", move |i: &mut I| {
        let request = bind_request(i)?;
        let device = device_id(i)?;
        let typ = bind_status(i)?;
        let opts = bind_options(i)?;
        let guid = guid(i)?;
        let uid = unique_id(i)?;
        Ok(BindInfo::new(request, device, typ, opts, guid, uid))
    })
    .parse_next(i)
}

fn bind_info_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("bind_info_payload", bind_info.map(PacketPayload::BindInfo)).parse_next(i)
}

fn query_parameter_value_payload<I>(i: &mut I) -> Output<ParameterConfigurationPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "query_parameter_value_payload",
        terminated(param_id, take(mem::size_of::<i32>()))
            .map(ParameterConfigurationPayload::QueryParameterValue),
    )
    .parse_next(i)
}

fn write_parameter_value_payload<I>(i: &mut I) -> Output<ParameterConfigurationPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("write_parameter_value_payload", move |i: &mut I| {
        let id = param_id(i)?;
        let value = param_value(i)?;
        Ok(ParameterConfigurationPayload::WriteParameterValue(
            id, value,
        ))
    })
    .parse_next(i)
}

fn parameter_configuration<I>(i: &mut I) -> Output<ParameterConfiguration>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("parameter_configuration", move |i: &mut I| {
        let request = parameter_configuration_request(i)?;
        let dest = typed_device_id(i)?;
        let payload = match request {
            ParameterConfigurationRequest::QueryParameterValue => query_parameter_value_payload,
            ParameterConfigurationRequest::WriteParameterValue => write_parameter_value_payload,
        }
        .parse_next(i)?;
        Ok(ParameterConfiguration::new(dest, payload))
    })
    .parse_next(i)
}

fn parameter_configuration_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "parameter_configuration_payload",
        parameter_configuration.map(PacketPayload::ParameterConfiguration),
    )
    .parse_next(i)
}

fn signal_quality<I>(i: &mut I) -> Output<SignalQuality>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("signal_quality", move |i: &mut I| {
        let request = signal_quality_request(i)?;
        let antenna_a = rssi(i)?;
        let antenna_b = rssi(i)?;
        let antenna_l = rssi(i)?;
        let antenna_r = rssi(i)?;
        Ok(SignalQuality::new(
            request, antenna_a, antenna_b, antenna_l, antenna_r,
        ))
    })
    .parse_next(i)
}

fn signal_quality_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "signal_quality_payload",
        signal_quality.map(PacketPayload::SignalQuality),
    )
    .parse_next(i)
}

fn telemetry_destination<I>(i: &mut I) -> Output<TelemetryDestination>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "telemetry_destination",
        le_u8.try_map(TelemetryDestination::try_from),
    )
    .parse_next(i)
}

fn telemetry_sensor_data<I>(i: &mut I) -> Output<TelemetrySensorData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("telemetry_sensor_data", move |i: &mut I| {
        let dest = telemetry_destination(i)?;
        let packet = telemetry_packet(i)?;
        Ok(TelemetrySensorData::new(dest, packet))
    })
    .parse_next(i)
}

fn telemetry_sensor_data_payload<I>(i: &mut I) -> Output<PacketPayload>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "telemetry_sensor_data_payload",
        telemetry_sensor_data.map(PacketPayload::TelemetrySensorData),
    )
    .parse_next(i)
}

#[derive(Debug, PartialEq)]
pub struct InvalidPacket(ContextError);

#[derive(Debug, PartialEq)]
pub enum Error {
    Incomplete,
    InvalidPacket(InvalidPacket),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Parser error: {:?}", self)
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

pub trait PacketExt<'i> {
    fn parse_packet(self) -> Result<(usize, PacketPayload), Error>;
}

impl<'i> PacketExt<'i> for Input<'i> {
    fn parse_packet(self) -> Result<(usize, PacketPayload), Error> {
        packet
            .parse_next(&mut Partial::new(self))
            .map_err(|err| match err {
                ErrMode::Incomplete(_) => Error::Incomplete,
                ErrMode::Backtrack(e) | ErrMode::Cut(e) => Error::InvalidPacket(InvalidPacket(e)),
            })
    }
}

#[derive(Debug)]
pub struct ReadBuffer {
    bytes: [u8; MAX_PACKET_LENGTH],
    length: usize,
}

impl ReadBuffer {
    pub const fn new() -> Self {
        Self {
            bytes: [0x0; MAX_PACKET_LENGTH],
            length: 0,
        }
    }

    /// Returns `true` if the buffer contains no bytes.
    pub fn is_empty(&self) -> bool {
        self.length == 0
    }

    /// Returns `true` if the buffer is unable to hold additional bytes.
    pub fn is_full(&self) -> bool {
        self.length == MAX_PACKET_LENGTH
    }

    /// Returns the number of unused bytes in the buffer.
    pub fn capacity(&self) -> usize {
        MAX_PACKET_LENGTH - self.length
    }

    /// Returns the number of bytes held in the buffer.
    pub fn len(&self) -> usize {
        self.length
    }

    /// Create a buffer containing the given bytes, up to a maximum of [`MAX_PACKET_LENGTH`] bytes.
    pub fn from_bytes(bytes: impl AsRef<[u8]>) -> Self {
        let mut buffer = Self::new();
        let bytes = bytes.as_ref();
        let len = cmp::min(bytes.len(), MAX_PACKET_LENGTH);
        buffer.bytes[..len].copy_from_slice(&bytes[..len]);
        buffer.length = len;
        buffer
    }

    /// Fill the buffer using a mutable slice referencing its remaining [`ReadBuffer::capacity`].
    pub fn fill<'b, 's, F: FnOnce(&'s mut [u8]) -> Result<usize, E>, E>(
        &'b mut self,
        f: F,
    ) -> Result<usize, E>
    where
        'b: 's,
    {
        if self.is_full() {
            Ok(0)
        } else {
            let len = f(&mut self.bytes[self.length..])?;
            self.length += len;
            Ok(len)
        }
    }

    fn next_packet(&self, start: usize) -> usize {
        if start >= self.length {
            self.length
        } else {
            let mut index = start;
            for byte in &self.bytes[index..self.length] {
                if *byte == Protocol::SRXL2 {
                    break;
                }
                index += 1;
            }
            index
        }
    }

    /// Attempt to read a [`PacketPayload`] from the buffer, consuming the bytes upon success.
    pub fn read(&mut self) -> Result<PacketPayload, Error> {
        if self.is_empty() {
            Err(Error::Incomplete)
        } else {
            let mut start = self.next_packet(0);
            let result: Result<PacketPayload, Error> =
                match (&self.bytes[start..self.length]).parse_packet() {
                    Ok((parsed, payload)) => {
                        start += parsed;
                        Ok(payload)
                    }
                    Err(err) => {
                        match err {
                            Error::Incomplete => {}
                            _ => start = self.next_packet(start + 1),
                        }
                        Err(err)
                    }
                };
            if start > 0 {
                if start == self.length {
                    self.length = 0;
                } else {
                    self.bytes.copy_within(start..self.length, 0);
                    self.length -= start;
                }
            }
            result
        }
    }
}

impl Default for ReadBuffer {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SAMPLE_PACKET: [u8; 14] = [
        0xA6, 0x21, 0x0E, 0x21, 0x40, 0x0A, 0x00, 0x03, 0x78, 0x56, 0x34, 0x12, 0xF3, 0x91,
    ];

    #[derive(Debug, PartialEq)]
    struct Never;

    fn filler<const LENGTH: usize>(
        data: [u8; LENGTH],
    ) -> impl FnOnce(&mut [u8]) -> Result<usize, Never> {
        move |b: &mut [u8]| {
            b[..LENGTH].copy_from_slice(&data);
            Ok(LENGTH)
        }
    }

    #[test]
    fn read_buffer_methods() {
        let mut buf = ReadBuffer::new();
        assert!(buf.is_empty());
        assert!(!buf.is_full());
        assert_eq!(buf.len(), 0);
        assert_eq!(buf.capacity(), MAX_PACKET_LENGTH);
        assert_eq!(buf.fill(filler([0x11, 0x22])), Ok(2));
        assert!(!buf.is_empty());
        assert!(!buf.is_full());
        assert_eq!(buf.capacity(), 78);
        assert_eq!(buf.len(), 2);
        assert_eq!(buf.length, 2);
        assert_eq!(buf.bytes[0], 0x11);
        assert_eq!(buf.bytes[1], 0x22);
        assert_eq!(buf.fill(filler(SAMPLE_PACKET.clone())), Ok(14));
        assert_eq!(buf.length, 16);
        assert_eq!(buf.fill(filler([0x33, 0x44])), Ok(2));
        assert_eq!(buf.length, 18);
        assert!(buf.read().is_ok());
        assert_eq!(buf.length, 2);
        assert_eq!(buf.bytes[0], 0x33);
        assert_eq!(buf.bytes[1], 0x44);
    }

    #[test]
    fn read_buffer_creation_from_array() {
        let buf = ReadBuffer::from_bytes([0x1, 0x2]);
        assert_eq!(buf.length, 2);
        assert_eq!(buf.bytes[0], 0x1);
        assert_eq!(buf.bytes[1], 0x2);
    }

    #[test]
    fn read_buffer_creation_from_slice() {
        let buf = ReadBuffer::from_bytes(&[0x3, 0x4, 0x5][..]);
        assert_eq!(buf.length, 3);
        assert_eq!(buf.bytes[0], 0x3);
        assert_eq!(buf.bytes[1], 0x4);
        assert_eq!(buf.bytes[2], 0x5);
    }

    #[test]
    fn read_buffer_creation_oversize_input() {
        let mut buf = ReadBuffer::from_bytes([0xFF; MAX_PACKET_LENGTH + 1]);
        assert!(buf.is_full());
        assert_eq!(buf.len(), MAX_PACKET_LENGTH);
        assert_eq!(buf.capacity(), 0);
        assert_eq!(buf.fill(filler([0xFF])), Ok(0));
    }

    #[test]
    fn read_empty_packet_data() {
        assert_eq!(ReadBuffer::new().read(), Err(Error::Incomplete));
    }

    #[test]
    fn read_truncated_packet_data() {
        for len in 1..SAMPLE_PACKET.len() - 1 {
            let mut buf = ReadBuffer::from_bytes(&SAMPLE_PACKET[..len]);
            assert_eq!(buf.len(), len);
            assert_eq!(buf.read(), Err(Error::Incomplete));
            assert_eq!(buf.len(), len);
        }
    }

    #[test]
    fn read_invalid_packet_checksum() {
        let mut bytes = SAMPLE_PACKET.clone();
        bytes[bytes.len() - 1] += 1;
        let mut buf = ReadBuffer::from_bytes(bytes);
        if let Err(Error::InvalidPacket(_)) = buf.read() {
            assert!(buf.is_empty());
        } else {
            unreachable!()
        }
    }

    #[test]
    fn read_invalid_payload_data() {
        let mut buf = ReadBuffer::from_bytes([0xA6, 0xCD, 0x06, 0x00, 0x03, 0xEF]);
        if let Err(Error::InvalidPacket(_)) = buf.read() {
            assert!(buf.is_empty());
        } else {
            unreachable!()
        }
    }

    #[test]
    fn read_sample_packet() {
        assert!(SAMPLE_PACKET.as_ref().parse_packet().is_ok());
    }
}
