pub mod convert;
pub mod ops;

use bitflags::bitflags;
use core::fmt;
use core::slice;
use heapless::Vec;

pub const MIN_PACKET_LENGTH: usize = 5;
pub const MAX_PACKET_LENGTH: usize = 80;

pub struct Protocol {}

impl Protocol {
    pub const SRXL2: u8 = 0xA6;
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum PacketType {
    Handshake,
    BindInfo,
    ParameterConfiguration,
    SignalQuality,
    TelemetrySensorData,
    ControlData,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PacketPayload {
    Handshake(Handshake),
    BindInfo(BindInfo),
    ParameterConfiguration(ParameterConfiguration),
    SignalQuality(SignalQuality),
    TelemetrySensorData(TelemetrySensorData),
    ControlData(ControlData),
}

impl PacketPayload {
    pub fn packet_type(&self) -> PacketType {
        match self {
            Self::Handshake(_) => PacketType::Handshake,
            Self::BindInfo(_) => PacketType::BindInfo,
            Self::ParameterConfiguration(_) => PacketType::ParameterConfiguration,
            Self::SignalQuality(_) => PacketType::SignalQuality,
            Self::TelemetrySensorData(_) => PacketType::TelemetrySensorData,
            Self::ControlData(_) => PacketType::ControlData,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct TypedDeviceID(DeviceType, UnitID);

impl TypedDeviceID {
    pub fn device_type(&self) -> DeviceType {
        self.0
    }

    pub fn unit_id(&self) -> UnitID {
        self.1
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum DeviceID {
    #[default]
    NotSpecified,
    Typed(TypedDeviceID),
    Broadcast,
    Reserved(u8),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum ServoPrefix {
    #[default]
    Low,
    High,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DeviceType {
    RemoteReceiver,
    Receiver,
    FlightController,
    ESC,
    SRXLServo(ServoPrefix),
    VTX,
}

impl DeviceType {
    pub fn default_id(&self) -> TypedDeviceID {
        TypedDeviceID(
            *self,
            UnitID(match self {
                Self::Receiver | Self::VTX => 0x1,
                _ => 0x0,
            }),
        )
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct UnitID(u8);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Priority(u8);

impl Default for Priority {
    fn default() -> Self {
        Self(10)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum BaudSupported {
    #[default]
    Only115200,
    Also400000,
}

bitflags! {
    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub struct DeviceOptions: u8 {
        const EnableTelemetryOverRF = 1 << 0;
        const SendsFullRangeTelemetry = 1 << 1;
        const ForwardProgrammingTarget = 1 << 2;
    }
}

impl Default for DeviceOptions {
    fn default() -> Self {
        Self::empty()
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct UniqueID(u32);

#[derive(Debug, PartialEq, Eq)]
pub struct Handshake {
    pub source: TypedDeviceID,
    pub destination: DeviceID,
    pub priority: Priority,
    pub baud_supported: BaudSupported,
    pub options: DeviceOptions,
    pub uid: UniqueID,
}

impl Handshake {
    pub fn new(
        source: TypedDeviceID,
        destination: DeviceID,
        priority: Priority,
        baud_supported: BaudSupported,
        options: DeviceOptions,
        uid: UniqueID,
    ) -> Self {
        Self {
            source,
            destination,
            priority,
            baud_supported,
            options,
            uid,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum BindRequest {
    EnterBindMode,
    RequestBindStatus,
    BoundDataReport,
    SetBindInfo,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DSM2 {
    TwentyTwoMillis,
    MC24,
    ElevenMillis,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DSMX {
    TwentyTwoMillis,
    ElevenMillis,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DSMR {
    ElevenOrTwentyTwoMillis,
    FivePointFiveMillis,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Air {
    DSM2(DSM2),
    DSMX(DSMX),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Surface {
    DSM2,
    DSMR(DSMR),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum BindStatus {
    #[default]
    NotBound,
    Air(Air),
    Surface(Surface),
}

bitflags! {
    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub struct BindOptions: u8 {
        const EnableTelemetryOverRF = 1 << 0;
        const AllowBindReplyOverRF = 1 << 1;
        const RequestUSPowerLevelForRFTransmits = 1 << 2;
    }
}

impl Default for BindOptions {
    fn default() -> Self {
        Self::empty()
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct GUID(u64);

#[derive(Debug, PartialEq, Eq)]
pub struct BindInfo {
    pub request: BindRequest,
    pub device: DeviceID,
    pub status: BindStatus,
    pub options: BindOptions,
    pub guid: GUID,
    pub uid: UniqueID,
}

impl BindInfo {
    pub fn new(
        request: BindRequest,
        device: DeviceID,
        status: BindStatus,
        options: BindOptions,
        guid: GUID,
        uid: UniqueID,
    ) -> Self {
        Self {
            request,
            device,
            status,
            options,
            guid,
            uid,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParameterConfigurationRequest {
    QueryParameterValue,
    WriteParameterValue,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct ParamID(u32);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct ParamValue(u32);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParameterConfigurationPayload {
    QueryParameterValue(ParamID),
    WriteParameterValue(ParamID, ParamValue),
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParameterConfiguration {
    pub destination: TypedDeviceID,
    pub payload: ParameterConfigurationPayload,
}

impl ParameterConfiguration {
    pub fn new(destination: TypedDeviceID, payload: ParameterConfigurationPayload) -> Self {
        Self {
            destination,
            payload,
        }
    }

    pub fn request(&self) -> ParameterConfigurationRequest {
        match self.payload {
            ParameterConfigurationPayload::QueryParameterValue(_) => {
                ParameterConfigurationRequest::QueryParameterValue
            }
            ParameterConfigurationPayload::WriteParameterValue(_, _) => {
                ParameterConfigurationRequest::WriteParameterValue
            }
        }
    }

    pub fn id(&self) -> ParamID {
        match self.payload {
            ParameterConfigurationPayload::QueryParameterValue(id)
            | ParameterConfigurationPayload::WriteParameterValue(id, _) => id,
        }
    }

    pub fn value(&self) -> Option<ParamValue> {
        match self.payload {
            ParameterConfigurationPayload::QueryParameterValue(_) => None,
            ParameterConfigurationPayload::WriteParameterValue(_, value) => Some(value),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum SignalQualityRequest {
    RequestQualityStatus,
    QualityStatusReport,
}

#[derive(Debug, PartialEq, Eq)]
pub struct SignalQuality {
    pub request: SignalQualityRequest,
    pub antenna_a: RSSI,
    pub antenna_b: RSSI,
    pub antenna_l: RSSI,
    pub antenna_r: RSSI,
}

impl SignalQuality {
    pub fn new(
        request: SignalQualityRequest,
        antenna_a: RSSI,
        antenna_b: RSSI,
        antenna_l: RSSI,
        antenna_r: RSSI,
    ) -> Self {
        Self {
            request,
            antenna_a,
            antenna_b,
            antenna_l,
            antenna_r,
        }
    }
}

pub const TELEMETRY_PACKET_LENGTH: usize = 16;

#[derive(Debug, PartialEq, Eq)]
pub struct TelemetryPacket([u8; TELEMETRY_PACKET_LENGTH]);

impl TelemetryPacket {
    pub fn new(data: impl Into<[u8; TELEMETRY_PACKET_LENGTH]>) -> Self {
        Self(data.into())
    }
}

impl Default for TelemetryPacket {
    fn default() -> Self {
        Self([
            0x0, 0x0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF,
        ])
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum TelemetryDestination {
    #[default]
    Disabled,
    Receiver(TypedDeviceID),
    HandshakeNeeded,
}

impl TelemetryDestination {
    pub fn is_receiver(&self) -> bool {
        matches!(self, Self::Receiver(_))
    }
}

impl From<TelemetryDestination> for u8 {
    fn from(telemetry_destination: TelemetryDestination) -> Self {
        match telemetry_destination {
            TelemetryDestination::Disabled => 0x0,
            TelemetryDestination::HandshakeNeeded => 0xFF,
            TelemetryDestination::Receiver(id) => id.into(),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct TelemetrySensorData {
    pub destination: TelemetryDestination,
    pub packet: TelemetryPacket,
}

impl TelemetrySensorData {
    pub fn new(destination: TelemetryDestination, packet: TelemetryPacket) -> Self {
        Self {
            destination,
            packet,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ControlDataCommand {
    ChannelData,
    FailsafeChannelData,
    VTXData,
    Reserved,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum ReplyID {
    #[default]
    NoDevice,
    SlaveDevice(TypedDeviceID),
    AllDevices,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum VTXBand {
    Fatshark,
    Raceband,
    E,
    B,
    A,
    Reserved(u8),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct VTXChannel(u8);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum VTXMode {
    Race,
    Pit,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum VTXRegion {
    USA = 0x0,
    EU = 0x1,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum VTXPower {
    Off,
    Milliwatts1_14,
    Milliwatts15_25,
    Milliwatts26_99,
    Milliwatts100_299,
    Milliwatts300_600,
    Milliwatts601Plus,
    ManualControl,
    DecimalMilliwatts(u16),
    NoChange,
}

#[derive(Debug, PartialEq, Eq)]
pub struct VTXData {
    pub band: VTXBand,
    pub channel: VTXChannel,
    pub mode: VTXMode,
    pub power: VTXPower,
    pub region: VTXRegion,
}

impl VTXData {
    pub fn new(
        band: VTXBand,
        channel: VTXChannel,
        mode: VTXMode,
        power: VTXPower,
        region: VTXRegion,
    ) -> Self {
        Self {
            band,
            channel,
            mode,
            power,
            region,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct RangeRemaining(i8);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct DecibelMilliwatts(i8);

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub enum RSSI {
    #[default]
    NoData,
    RangeRemaining(RangeRemaining),
    DecibelMilliwatts(DecibelMilliwatts),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub struct FrameLosses(u16);

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct ChannelValue(u16);

impl ChannelValue {
    /// Minimum allowed channel value
    pub const MIN: Self = Self(0x0);

    /// Approximately -100% on Spektrum transmitter
    pub const NEG_100: Self = Self(0x2AA0);

    /// Center position (0%) on Spektrum transmitter
    pub const CENTER: Self = Self(0x8000);

    /// Approximately 100% on Spektrum transmitter
    pub const POS_100: Self = Self(0xD554);

    /// Maximum allowed channel value
    pub const MAX: Self = Self(0xFFFC);
}

pub type ChannelValuesVec = Vec<ChannelValue, { u32::BITS as usize }>;

#[derive(PartialEq, Eq, Clone)]
pub struct ChannelValues {
    mask: u32,
    values: ChannelValuesVec,
}

impl ChannelValues {
    pub fn mask(&self) -> u32 {
        self.mask
    }

    pub fn is_empty(&self) -> bool {
        self.values.is_empty()
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn pairs(&self) -> ChannelValuesIterator {
        ChannelValuesIterator::new(self)
    }

    pub fn get(&self, channel: u8) -> Option<ChannelValue> {
        let channel = channel - 1;
        let mut values_offset: usize = 0;
        for mask_bit in 0..=channel {
            if values_offset == self.values.len() {
                break;
            }
            if self.mask >> mask_bit & 0x1 == 0x1 {
                if mask_bit == channel {
                    return Some(self.values[values_offset]);
                }
                values_offset += 1;
            }
        }
        None
    }
}

impl fmt::Debug for ChannelValues {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ChannelValues({:b}, {:?})", self.mask, self.values)
    }
}

#[derive(Debug)]
pub struct ChannelValuesIterator<'v> {
    values: &'v ChannelValues,
    mask_bit: u8,
}

impl<'v> ChannelValuesIterator<'v> {
    fn new(values: &'v ChannelValues) -> Self {
        Self {
            values,
            mask_bit: 0,
        }
    }
}

impl<'v> Iterator for ChannelValuesIterator<'v> {
    type Item = (u8, Option<ChannelValue>);

    fn next(&mut self) -> Option<Self::Item> {
        if self.mask_bit == u32::BITS as u8 {
            None
        } else {
            self.mask_bit += 1;
            Some((self.mask_bit, self.values.get(self.mask_bit)))
        }
    }
}

impl<'v> ExactSizeIterator for ChannelValuesIterator<'v> {
    fn len(&self) -> usize {
        self.values.len()
    }
}

impl<'cv> IntoIterator for &'cv ChannelValues {
    type Item = &'cv ChannelValue;
    type IntoIter = slice::Iter<'cv, ChannelValue>;

    fn into_iter(self) -> Self::IntoIter {
        self.values[..].iter()
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct ChannelData {
    pub rssi: RSSI,
    pub frame_losses: FrameLosses,
    pub link_state: RFLinkState,
}

#[derive(Debug, PartialEq, Eq)]
pub enum RFLinkState {
    Nominal(ChannelValues),
    Fade,
}

impl ChannelData {
    pub fn new(rssi: RSSI, frame_losses: FrameLosses, link_state: RFLinkState) -> Self {
        Self {
            rssi,
            frame_losses,
            link_state,
        }
    }

    pub fn values(&self) -> Option<&ChannelValues> {
        match &self.link_state {
            RFLinkState::Nominal(cv) => Some(cv),
            RFLinkState::Fade => None,
        }
    }

    pub fn get(&self, channel: u8) -> Option<ChannelValue> {
        match &self.link_state {
            RFLinkState::Nominal(cv) => cv.get(channel),
            RFLinkState::Fade => None,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub struct Holds(u16);

#[derive(Debug, PartialEq, Eq)]
pub struct FailsafeChannelData {
    pub rssi_min: RSSI,
    pub holds: Holds,
    pub values: ChannelValues,
}

impl FailsafeChannelData {
    pub fn new(rssi_min: RSSI, holds: Holds, values: ChannelValues) -> Self {
        Self {
            rssi_min,
            holds,
            values,
        }
    }

    pub fn values(&self) -> &ChannelValues {
        &self.values
    }

    pub fn get(&self, channel: u8) -> Option<ChannelValue> {
        self.values.get(channel)
    }
}

pub const CONTROL_DATA_PAYLOAD_LENGTH: usize = 64;

#[derive(Debug, PartialEq, Eq)]
pub enum ControlDataPayload {
    ChannelData(ChannelData),
    FailsafeChannelData(FailsafeChannelData),
    VTXData(VTXData),
    Reserved(Vec<u8, CONTROL_DATA_PAYLOAD_LENGTH>),
}

#[derive(Debug, PartialEq, Eq)]
pub struct ControlData {
    pub reply_id: ReplyID,
    pub payload: ControlDataPayload,
}

impl ControlData {
    pub fn new(reply_id: ReplyID, payload: ControlDataPayload) -> Self {
        Self { reply_id, payload }
    }

    pub fn command(&self) -> ControlDataCommand {
        match self.payload {
            ControlDataPayload::ChannelData(_) => ControlDataCommand::ChannelData,
            ControlDataPayload::FailsafeChannelData(_) => ControlDataCommand::FailsafeChannelData,
            ControlDataPayload::VTXData(_) => ControlDataCommand::VTXData,
            ControlDataPayload::Reserved(_) => ControlDataCommand::Reserved,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn typed_device_id_methods() {
        let id = TypedDeviceID::try_from(0x21).unwrap();
        assert_eq!(id.device_type(), DeviceType::Receiver);
        assert_eq!(id.unit_id(), UnitID(0x1));
    }

    #[test]
    fn device_id_methods() {
        assert_eq!(DeviceID::default(), DeviceID::NotSpecified);
    }

    #[test]
    fn servo_prefix_methods() {
        assert_eq!(ServoPrefix::default(), ServoPrefix::Low);
    }

    #[test]
    fn device_type_methods() {
        for dt in [
            DeviceType::RemoteReceiver,
            DeviceType::FlightController,
            DeviceType::ESC,
            DeviceType::SRXLServo(ServoPrefix::Low),
            DeviceType::SRXLServo(ServoPrefix::High),
        ] {
            assert_eq!(dt.default_id(), TypedDeviceID(dt, UnitID(0x0)));
        }
        for dt in [DeviceType::Receiver, DeviceType::VTX] {
            assert_eq!(dt.default_id(), TypedDeviceID(dt, UnitID(0x1)));
        }
    }

    #[test]
    fn baud_supported_methods() {
        assert_eq!(BaudSupported::default(), BaudSupported::Only115200)
    }

    #[test]
    fn priority_methods() {
        assert_eq!(Priority::default(), Priority(10))
    }

    #[test]
    fn device_option_methods() {
        assert_eq!(DeviceOptions::default(), DeviceOptions::empty())
    }

    #[test]
    fn bind_option_methods() {
        assert_eq!(BindOptions::default(), BindOptions::empty())
    }

    #[test]
    fn telemtry_packet_methods() {
        let tp = TelemetryPacket::default();
        assert_eq!(&tp.0[..2], [0x0; 2]);
        assert_eq!(&tp.0[2..], [0xFF; 14]);
    }

    #[test]
    fn telemetry_destination_methods() {
        assert_eq!(
            TelemetryDestination::default(),
            TelemetryDestination::Disabled
        );
        assert!(TelemetryDestination::Receiver(DeviceType::Receiver.default_id()).is_receiver());
        assert!(!TelemetryDestination::Disabled.is_receiver());
        assert!(!TelemetryDestination::HandshakeNeeded.is_receiver());
    }

    #[test]
    fn reply_id_methods() {
        assert_eq!(ReplyID::default(), ReplyID::NoDevice);
    }

    #[test]
    fn rssi_methods() {
        assert_eq!(RSSI::default(), RSSI::NoData);
    }

    #[test]
    fn channel_values_methods() {
        let cv = ChannelValues {
            mask: 0b10011,
            values: Vec::from_iter([
                ChannelValue(0x0),
                ChannelValue(0x8000),
                ChannelValue(0xFFFC),
            ]),
        };
        assert!(!cv.is_empty());
        assert_eq!(cv.len(), 3);
        let mut values = cv.into_iter();
        assert_eq!(values.len(), 3);
        let mut pairs = cv.pairs();
        assert_eq!(cv.get(1), Some(ChannelValue(0x0)));
        assert_eq!(values.next(), Some(&ChannelValue(0x0)));
        assert_eq!(pairs.next(), Some((1, Some(ChannelValue(0x0)))));
        assert_eq!(cv.get(2), Some(ChannelValue(0x8000)));
        assert_eq!(values.next(), Some(&ChannelValue(0x8000)));
        assert_eq!(pairs.next(), Some((2, Some(ChannelValue(0x8000)))));
        for c in [3, 4] {
            assert_eq!(cv.get(c), None);
            assert_eq!(pairs.next(), Some((c, None)));
        }
        assert_eq!(cv.get(5), Some(ChannelValue(0xFFFC)));
        assert_eq!(values.next(), Some(&ChannelValue(0xFFFC)));
        assert_eq!(pairs.next(), Some((5, Some(ChannelValue(0xFFFC)))));
        for c in 6..=32 {
            assert_eq!(cv.get(c), None);
            assert_eq!(pairs.next(), Some((c, None)));
        }
        assert_eq!(values.next(), None);
        assert_eq!(pairs.next(), None);
    }

    #[test]
    fn channel_data_methods() {
        let cd = ChannelData::new(
            RSSI::from(-85),
            FrameLosses(7),
            RFLinkState::Nominal(ChannelValues {
                mask: 0b10011,
                values: Vec::from_iter([
                    ChannelValue(0x0),
                    ChannelValue(0x8000),
                    ChannelValue(0xFFFC),
                ]),
            }),
        );
        assert!(cd.values().is_some());
        assert_eq!(cd.get(1), Some(ChannelValue(0x0)));
        assert_eq!(cd.get(2), Some(ChannelValue(0x8000)));
        assert_eq!(cd.get(5), Some(ChannelValue(0xFFFC)));
        for c in (6..=32).chain([3, 4]) {
            assert_eq!(cd.get(c), None);
        }
    }

    #[test]
    fn channel_data_no_values() {
        let cd = ChannelData::new(RSSI::from(99), FrameLosses(0), RFLinkState::Fade);
        assert_eq!(cd.values(), None);
        for c in 1..=32 {
            assert_eq!(cd.get(c), None);
        }
    }
}
