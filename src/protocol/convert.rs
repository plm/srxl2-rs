use super::*;

use paste::paste;

#[cfg(feature = "std")]
use std::error::Error;

impl From<PacketType> for u8 {
    fn from(packet_type: PacketType) -> Self {
        match packet_type {
            PacketType::Handshake => 0x21,
            PacketType::BindInfo => 0x41,
            PacketType::ParameterConfiguration => 0x50,
            PacketType::SignalQuality => 0x55,
            PacketType::TelemetrySensorData => 0x80,
            PacketType::ControlData => 0xCD,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`PacketType`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidPacketType(pub u8);

impl fmt::Display for InvalidPacketType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid PacketType 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidPacketType {}

impl TryFrom<u8> for PacketType {
    type Error = InvalidPacketType;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x21 => Ok(Self::Handshake),
            0x41 => Ok(Self::BindInfo),
            0x50 => Ok(Self::ParameterConfiguration),
            0x55 => Ok(Self::SignalQuality),
            0x80 => Ok(Self::TelemetrySensorData),
            0xCD => Ok(Self::ControlData),
            _ => Err(InvalidPacketType(value)),
        }
    }
}

impl DeviceType {
    pub fn with_id(&self, value: u8) -> Result<TypedDeviceID, InvalidTypedDeviceID> {
        let lower = value & 0xF;
        match self {
            // Receiver and VTX do not allow unit ID of 0x0
            Self::Receiver | Self::VTX if lower == 0x0 => {
                Err(InvalidTypedDeviceID((u8::from(*self) << 4) + lower))
            }
            _ => Ok(TypedDeviceID(*self, UnitID(lower))),
        }
    }
}

impl From<TypedDeviceID> for u8 {
    fn from(typed_device_id: TypedDeviceID) -> Self {
        (Self::from(typed_device_id.0) << 4) + Self::from(typed_device_id.1)
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`TypedDeviceID`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidTypedDeviceID(pub u8);

impl fmt::Display for InvalidTypedDeviceID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid TypedDeviceID 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidTypedDeviceID {}

impl TryFrom<u8> for TypedDeviceID {
    type Error = InvalidTypedDeviceID;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match DeviceType::try_from(value >> 4) {
            Ok(device_type) => device_type.with_id(value & 0xF),
            Err(_) => Err(InvalidTypedDeviceID(value)),
        }
    }
}

impl From<DeviceID> for u8 {
    fn from(device_id: DeviceID) -> Self {
        match device_id {
            DeviceID::NotSpecified => 0x0,
            DeviceID::Typed(typed_device_id) => typed_device_id.into(),
            DeviceID::Broadcast => 0xFF,
            DeviceID::Reserved(byte) => byte,
        }
    }
}

impl From<TypedDeviceID> for DeviceID {
    fn from(typed_device_id: TypedDeviceID) -> Self {
        Self::Typed(typed_device_id)
    }
}

impl From<DeviceType> for u8 {
    fn from(device_type: DeviceType) -> Self {
        match device_type {
            DeviceType::RemoteReceiver => 0x1,
            DeviceType::Receiver => 0x2,
            DeviceType::FlightController => 0x3,
            DeviceType::ESC => 0x4,
            DeviceType::SRXLServo(ServoPrefix::Low) => 0x6,
            DeviceType::SRXLServo(ServoPrefix::High) => 0x7,
            DeviceType::VTX => 0x8,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`DeviceType`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidDeviceType(pub u8);

impl fmt::Display for InvalidDeviceType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid DeviceType 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidDeviceType {}

impl TryFrom<u8> for DeviceType {
    type Error = InvalidDeviceType;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x1 => Ok(Self::RemoteReceiver),
            0x2 => Ok(Self::Receiver),
            0x3 => Ok(Self::FlightController),
            0x4 => Ok(Self::ESC),
            0x6 => Ok(Self::SRXLServo(ServoPrefix::Low)),
            0x7 => Ok(Self::SRXLServo(ServoPrefix::High)),
            0x8 => Ok(Self::VTX),
            _ => Err(InvalidDeviceType(value)),
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`DeviceID`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidDeviceID(pub u8);

impl fmt::Display for InvalidDeviceID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid DeviceID 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidDeviceID {}

impl From<InvalidTypedDeviceID> for InvalidDeviceID {
    fn from(err: InvalidTypedDeviceID) -> Self {
        Self(err.0)
    }
}

impl TryFrom<u8> for DeviceID {
    type Error = InvalidDeviceID;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        if value == 0x0 {
            Ok(Self::NotSpecified)
        } else if value == 0xFF {
            Ok(Self::Broadcast)
        } else if let Ok(typed_device_id) = TypedDeviceID::try_from(value) {
            Ok(Self::Typed(typed_device_id))
        } else {
            match value >> 4 {
                0x5 | 0x9 | 0xA | 0xB | 0xC | 0xD | 0xE => Ok(Self::Reserved(value)),
                _ => Err(InvalidDeviceID(value)),
            }
        }
    }
}

impl From<UnitID> for u8 {
    fn from(unit_id: UnitID) -> Self {
        unit_id.0
    }
}

impl From<Priority> for u8 {
    fn from(priority: Priority) -> Self {
        priority.0
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`Priority`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidPriority(pub u8);

impl fmt::Display for InvalidPriority {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid Priority {}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidPriority {}

impl TryFrom<u8> for Priority {
    type Error = InvalidPriority;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            1..=100 => Ok(Self(value)),
            _ => Err(InvalidPriority(value)),
        }
    }
}

impl From<BaudSupported> for u8 {
    fn from(baud_supported: BaudSupported) -> Self {
        match baud_supported {
            BaudSupported::Only115200 => 0x0,
            BaudSupported::Also400000 => 0x1,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`BaudSupported`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidBaudSupported(pub u8);

impl fmt::Display for InvalidBaudSupported {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid BaudSupported 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidBaudSupported {}

impl TryFrom<u8> for BaudSupported {
    type Error = InvalidBaudSupported;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::Only115200),
            0x1 => Ok(Self::Also400000),
            _ => Err(InvalidBaudSupported(value)),
        }
    }
}

impl From<DeviceOptions> for u8 {
    fn from(device_options: DeviceOptions) -> Self {
        device_options.bits()
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`DeviceOptions`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidDeviceOptions(pub u8);

impl fmt::Display for InvalidDeviceOptions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid DeviceOptions {:b}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidDeviceOptions {}

impl TryFrom<u8> for DeviceOptions {
    type Error = InvalidDeviceOptions;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Self::from_bits(value).ok_or(InvalidDeviceOptions(value))
    }
}

impl From<UniqueID> for u32 {
    fn from(uid: UniqueID) -> Self {
        uid.0
    }
}

impl From<u32> for UniqueID {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl From<Handshake> for PacketPayload {
    fn from(handshake: Handshake) -> Self {
        Self::Handshake(handshake)
    }
}

impl From<BindRequest> for u8 {
    fn from(bind_request: BindRequest) -> Self {
        match bind_request {
            BindRequest::EnterBindMode => 0xEB,
            BindRequest::RequestBindStatus => 0xB5,
            BindRequest::BoundDataReport => 0xDB,
            BindRequest::SetBindInfo => 0x5B,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`BindRequest`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidBindRequest(pub u8);

impl fmt::Display for InvalidBindRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid BindRequest 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidBindRequest {}

impl TryFrom<u8> for BindRequest {
    type Error = InvalidBindRequest;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0xEB => Ok(Self::EnterBindMode),
            0xB5 => Ok(Self::RequestBindStatus),
            0xDB => Ok(Self::BoundDataReport),
            0x5B => Ok(Self::SetBindInfo),
            _ => Err(InvalidBindRequest(value)),
        }
    }
}

impl From<BindStatus> for u8 {
    fn from(bind_type: BindStatus) -> Self {
        match bind_type {
            BindStatus::NotBound => 0x00,
            BindStatus::Air(Air::DSM2(DSM2::TwentyTwoMillis)) => 0x01,
            BindStatus::Air(Air::DSM2(DSM2::MC24)) => 0x02,
            BindStatus::Air(Air::DSM2(DSM2::ElevenMillis)) => 0x12,
            BindStatus::Air(Air::DSMX(DSMX::TwentyTwoMillis)) => 0xA2,
            BindStatus::Air(Air::DSMX(DSMX::ElevenMillis)) => 0xB2,
            BindStatus::Surface(Surface::DSM2) => 0x63,
            BindStatus::Surface(Surface::DSMR(DSMR::ElevenOrTwentyTwoMillis)) => 0xE2,
            BindStatus::Surface(Surface::DSMR(DSMR::FivePointFiveMillis)) => 0xE4,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`BindStatus`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidBindStatus(pub u8);

impl fmt::Display for InvalidBindStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid BindStatus 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidBindStatus {}

impl TryFrom<u8> for BindStatus {
    type Error = InvalidBindStatus;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x00 => Ok(Self::NotBound),
            0x01 => Ok(Self::Air(Air::DSM2(DSM2::TwentyTwoMillis))),
            0x02 => Ok(Self::Air(Air::DSM2(DSM2::MC24))),
            0x12 => Ok(Self::Air(Air::DSM2(DSM2::ElevenMillis))),
            0xA2 => Ok(Self::Air(Air::DSMX(DSMX::TwentyTwoMillis))),
            0xB2 => Ok(Self::Air(Air::DSMX(DSMX::ElevenMillis))),
            0x63 => Ok(Self::Surface(Surface::DSM2)),
            0xE2 => Ok(Self::Surface(Surface::DSMR(DSMR::ElevenOrTwentyTwoMillis))),
            0xE4 => Ok(Self::Surface(Surface::DSMR(DSMR::FivePointFiveMillis))),
            _ => Err(InvalidBindStatus(value)),
        }
    }
}

impl From<BindOptions> for u8 {
    fn from(bind_options: BindOptions) -> Self {
        bind_options.bits()
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`BindOptions`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidBindOptions(pub u8);

impl fmt::Display for InvalidBindOptions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid BindOptions {:b}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidBindOptions {}

impl TryFrom<u8> for BindOptions {
    type Error = InvalidBindOptions;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Self::from_bits(value).ok_or(InvalidBindOptions(value))
    }
}

impl From<BindInfo> for PacketPayload {
    fn from(bind_info: BindInfo) -> Self {
        Self::BindInfo(bind_info)
    }
}

impl From<ParameterConfigurationRequest> for u8 {
    fn from(param_config_request: ParameterConfigurationRequest) -> Self {
        match param_config_request {
            ParameterConfigurationRequest::QueryParameterValue => 0x50,
            ParameterConfigurationRequest::WriteParameterValue => 0x57,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`ParameterConfigurationRequest`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidParameterConfigurationRequest(pub u8);

impl fmt::Display for InvalidParameterConfigurationRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid ParameterConfigurationRequest 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidParameterConfigurationRequest {}

impl TryFrom<u8> for ParameterConfigurationRequest {
    type Error = InvalidParameterConfigurationRequest;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x50 => Ok(Self::QueryParameterValue),
            0x57 => Ok(Self::WriteParameterValue),
            _ => Err(InvalidParameterConfigurationRequest(value)),
        }
    }
}

impl From<ParamID> for u32 {
    fn from(param_id: ParamID) -> Self {
        param_id.0
    }
}

impl From<u32> for ParamID {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

macro_rules! from_param_value {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        impl From<ParamValue> for $t {
            fn from(param_value: ParamValue) -> Self {
                (param_value.0 as u32) as Self
            }
        }
        }
        )+
    }
}

from_param_value! { i8 i16 i32 u8 u16 u32 }

macro_rules! into_param_value {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        impl From<$t> for ParamValue {
            fn from(value: $t) -> Self {
                Self(value as u32)
            }
        }
        }
        )+
    }
}

into_param_value! { i8 i16 i32 u8 u16 u32 }

impl From<GUID> for u64 {
    fn from(guid: GUID) -> Self {
        guid.0
    }
}

impl From<u64> for GUID {
    fn from(value: u64) -> Self {
        Self(value)
    }
}

impl From<ParameterConfiguration> for PacketPayload {
    fn from(parameter_configuration: ParameterConfiguration) -> Self {
        Self::ParameterConfiguration(parameter_configuration)
    }
}

impl From<SignalQualityRequest> for u8 {
    fn from(signal_quality_request: SignalQualityRequest) -> Self {
        match signal_quality_request {
            SignalQualityRequest::RequestQualityStatus => 0x52,
            SignalQualityRequest::QualityStatusReport => 0x53,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`SignalQualityRequest`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidSignalQualityRequest(pub u8);

impl fmt::Display for InvalidSignalQualityRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid SignalQualityRequest 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidSignalQualityRequest {}

impl TryFrom<u8> for SignalQualityRequest {
    type Error = InvalidSignalQualityRequest;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x52 => Ok(Self::RequestQualityStatus),
            0x53 => Ok(Self::QualityStatusReport),
            _ => Err(InvalidSignalQualityRequest(value)),
        }
    }
}

impl From<SignalQuality> for PacketPayload {
    fn from(signal_quality: SignalQuality) -> Self {
        Self::SignalQuality(signal_quality)
    }
}

impl<'p> From<&'p TelemetryPacket> for &'p [u8] {
    fn from(packet: &'p TelemetryPacket) -> Self {
        &packet.0[..]
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`TelemetryDestination`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidTelemetryDestination(pub InvalidTypedDeviceID);

impl fmt::Display for InvalidTelemetryDestination {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid TelemetryDestination: {}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidTelemetryDestination {}

impl TryFrom<u8> for TelemetryDestination {
    type Error = InvalidTelemetryDestination;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::Disabled),
            0xFF => Ok(Self::HandshakeNeeded),
            _ => TypedDeviceID::try_from(value)
                .map(TelemetryDestination::Receiver)
                .map_err(InvalidTelemetryDestination),
        }
    }
}

impl From<TelemetrySensorData> for PacketPayload {
    fn from(telemetry_sensor_data: TelemetrySensorData) -> Self {
        Self::TelemetrySensorData(telemetry_sensor_data)
    }
}

impl From<ControlDataCommand> for u8 {
    fn from(control_data_command: ControlDataCommand) -> Self {
        match control_data_command {
            ControlDataCommand::ChannelData => 0x0,
            ControlDataCommand::FailsafeChannelData => 0x1,
            ControlDataCommand::VTXData => 0x2,
            ControlDataCommand::Reserved => 0x3,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`ControlDataCommand`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidControlDataCommand(pub u8);

impl fmt::Display for InvalidControlDataCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid ControlDataCommand 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidControlDataCommand {}

impl TryFrom<u8> for ControlDataCommand {
    type Error = InvalidControlDataCommand;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::ChannelData),
            0x1 => Ok(Self::FailsafeChannelData),
            0x2 => Ok(Self::VTXData),
            0x3 => Ok(Self::Reserved),
            _ => Err(InvalidControlDataCommand(value)),
        }
    }
}

impl From<ReplyID> for u8 {
    fn from(reply_id: ReplyID) -> Self {
        match reply_id {
            ReplyID::NoDevice => 0x0,
            ReplyID::SlaveDevice(device_id) => device_id.into(),
            ReplyID::AllDevices => 0xFF,
        }
    }
}

impl From<TypedDeviceID> for ReplyID {
    fn from(typed_device_id: TypedDeviceID) -> Self {
        Self::SlaveDevice(typed_device_id)
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`ReplyID`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidReplyID(pub InvalidTypedDeviceID);

impl fmt::Display for InvalidReplyID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Invalid ReplyID due to invalid TypedDeviceID 0x{:X}",
            self.0 .0
        )
    }
}

#[cfg(feature = "std")]
impl Error for InvalidReplyID {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.0)
    }
}

impl TryFrom<u8> for ReplyID {
    type Error = InvalidReplyID;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::NoDevice),
            0xFF => Ok(Self::AllDevices),
            _ => TypedDeviceID::try_from(value)
                .map(ReplyID::SlaveDevice)
                .map_err(InvalidReplyID),
        }
    }
}

impl From<VTXBand> for u8 {
    fn from(vtx_band: VTXBand) -> Self {
        match vtx_band {
            VTXBand::Fatshark => 0x0,
            VTXBand::Raceband => 0x1,
            VTXBand::E => 0x2,
            VTXBand::B => 0x3,
            VTXBand::A => 0x4,
            VTXBand::Reserved(v) => v,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`VTXBand`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidVTXBand(pub u8);

impl fmt::Display for InvalidVTXBand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid VTXBand 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidVTXBand {}

impl TryFrom<u8> for VTXBand {
    type Error = InvalidVTXBand;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::Fatshark),
            0x1 => Ok(Self::Raceband),
            0x2 => Ok(Self::E),
            0x3 => Ok(Self::B),
            0x4 => Ok(Self::A),
            0x5..=0x7 => Ok(Self::Reserved(value)),
            _ => Err(InvalidVTXBand(value)),
        }
    }
}

impl From<VTXChannel> for u8 {
    fn from(vtx_channel: VTXChannel) -> Self {
        vtx_channel.0
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`VTXChannel`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidVTXChannel(pub u8);

impl fmt::Display for InvalidVTXChannel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid VTXChannel 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidVTXChannel {}

impl TryFrom<u8> for VTXChannel {
    type Error = InvalidVTXChannel;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        if value <= 0x7 {
            Ok(Self(value))
        } else {
            Err(InvalidVTXChannel(value))
        }
    }
}

impl From<VTXMode> for u8 {
    fn from(vtx_mode: VTXMode) -> Self {
        match vtx_mode {
            VTXMode::Race => 0x0,
            VTXMode::Pit => 0x1,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`VTXMode`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidVTXMode(pub u8);

impl fmt::Display for InvalidVTXMode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid VTXMode 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidVTXMode {}

impl TryFrom<u8> for VTXMode {
    type Error = InvalidVTXMode;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::Race),
            0x1 => Ok(Self::Pit),
            _ => Err(InvalidVTXMode(value)),
        }
    }
}

impl From<VTXRegion> for u8 {
    fn from(vtx_region: VTXRegion) -> Self {
        match vtx_region {
            VTXRegion::USA => 0x0,
            VTXRegion::EU => 0x1,
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`VTXRegion`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidVTXRegion(pub u8);

impl fmt::Display for InvalidVTXRegion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid VTXRegion 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidVTXRegion {}

impl TryFrom<u8> for VTXRegion {
    type Error = InvalidVTXRegion;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x0 => Ok(Self::USA),
            0x1 => Ok(Self::EU),
            _ => Err(InvalidVTXRegion(value)),
        }
    }
}

impl From<VTXPower> for (u8, u16) {
    fn from(vtx_power: VTXPower) -> Self {
        match vtx_power {
            VTXPower::Off => (0x0, 0xFFFF),
            VTXPower::Milliwatts1_14 => (0x1, 0xFFFF),
            VTXPower::Milliwatts15_25 => (0x2, 0xFFFF),
            VTXPower::Milliwatts26_99 => (0x3, 0xFFFF),
            VTXPower::Milliwatts100_299 => (0x4, 0xFFFF),
            VTXPower::Milliwatts300_600 => (0x5, 0xFFFF),
            VTXPower::Milliwatts601Plus => (0x6, 0xFFFF),
            VTXPower::ManualControl => (0x7, 0xFFFF),
            VTXPower::DecimalMilliwatts(v) => (0xFF, v),
            VTXPower::NoChange => (0xFF, 0xFFFF),
        }
    }
}

/// An unsupported [`u8`] value prohibited creation of a [`VTXPower`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidVTXPower(pub u8, pub u16);

impl fmt::Display for InvalidVTXPower {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Invalid VTXPower 0x{:X} with decimal 0x{:X}",
            self.0, self.1
        )
    }
}

#[cfg(feature = "std")]
impl Error for InvalidVTXPower {}

impl TryFrom<(u8, u16)> for VTXPower {
    type Error = InvalidVTXPower;

    fn try_from(value: (u8, u16)) -> Result<Self, Self::Error> {
        match value {
            (0x0, _) => Ok(Self::Off),
            (0x1, _) => Ok(Self::Milliwatts1_14),
            (0x2, _) => Ok(Self::Milliwatts15_25),
            (0x3, _) => Ok(Self::Milliwatts26_99),
            (0x4, _) => Ok(Self::Milliwatts100_299),
            (0x5, _) => Ok(Self::Milliwatts300_600),
            (0x6, _) => Ok(Self::Milliwatts601Plus),
            (0x7, _) => Ok(Self::ManualControl),
            (0xFF, decimal) if decimal != 0xFFFF => Ok(Self::DecimalMilliwatts(decimal)),
            (0xFF, 0xFFFF) => Ok(Self::NoChange),
            (power, decimal) => Err(InvalidVTXPower(power, decimal)),
        }
    }
}

impl From<RangeRemaining> for i8 {
    fn from(range_remaining: RangeRemaining) -> Self {
        range_remaining.0
    }
}

impl From<DecibelMilliwatts> for i8 {
    fn from(decibel_milliwatts: DecibelMilliwatts) -> Self {
        decibel_milliwatts.0
    }
}

impl From<RSSI> for i8 {
    fn from(rssi: RSSI) -> Self {
        match rssi {
            RSSI::NoData => 0,
            RSSI::RangeRemaining(v) => v.into(),
            RSSI::DecibelMilliwatts(v) => v.into(),
        }
    }
}

impl From<i8> for RSSI {
    fn from(value: i8) -> Self {
        match value {
            0 => Self::NoData,
            1..=i8::MAX => Self::RangeRemaining(RangeRemaining(value)),
            i8::MIN..=-1 => Self::DecibelMilliwatts(DecibelMilliwatts(value)),
        }
    }
}

impl From<FrameLosses> for u16 {
    fn from(frame_losses: FrameLosses) -> Self {
        frame_losses.0
    }
}

impl From<u16> for FrameLosses {
    fn from(value: u16) -> Self {
        Self(value)
    }
}

impl From<ChannelValue> for u16 {
    fn from(channel_value: ChannelValue) -> Self {
        channel_value.0
    }
}

impl fmt::Debug for ChannelValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "0x{:X}", self.0)
    }
}

/// An unsupported [`u16`]  a [`ChannelValue`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidChannelValue(pub u16);

impl fmt::Display for InvalidChannelValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid channel value 0x{:X}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidChannelValue {}

impl TryFrom<u16> for ChannelValue {
    type Error = InvalidChannelValue;

    fn try_from(value: u16) -> Result<Self, Self::Error> {
        match value {
            v if v & !0b11 == v => Ok(Self(v)),
            _ => Err(InvalidChannelValue(value)),
        }
    }
}

/// An unequal masked bit count and channel count prohibited creation of a [`ChannelValues`].
#[derive(Debug, PartialEq, Eq)]
pub struct InvalidChannelValues {
    pub bits: u8,
    pub values: usize,
}

impl fmt::Display for InvalidChannelValues {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Invalid channel values ({} mask bits =/= {} values)",
            self.bits, self.values
        )
    }
}

#[cfg(feature = "std")]
impl Error for InvalidChannelValues {}

impl TryFrom<(u32, ChannelValuesVec)> for ChannelValues {
    type Error = InvalidChannelValues;

    fn try_from((mask, values): (u32, ChannelValuesVec)) -> Result<Self, Self::Error> {
        let bits_set = mask.count_ones() as u8;
        if bits_set == values.len() as u8 {
            Ok(Self { mask, values })
        } else {
            Err(InvalidChannelValues {
                bits: bits_set,
                values: values.len(),
            })
        }
    }
}

impl From<ChannelValues> for RFLinkState {
    fn from(channel_values: ChannelValues) -> Self {
        match channel_values.len() {
            0 => Self::Fade,
            _ => Self::Nominal(channel_values),
        }
    }
}

impl TryFrom<(u32, ChannelValuesVec)> for RFLinkState {
    type Error = InvalidChannelValues;

    fn try_from(pair: (u32, ChannelValuesVec)) -> Result<Self, Self::Error> {
        ChannelValues::try_from(pair).map(Self::from)
    }
}

impl From<Holds> for u16 {
    fn from(holds: Holds) -> Self {
        holds.0
    }
}

impl From<u16> for Holds {
    fn from(v: u16) -> Self {
        Self(v)
    }
}

impl From<ControlData> for PacketPayload {
    fn from(control_data: ControlData) -> Self {
        Self::ControlData(control_data)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn packet_type_conversion() {
        assert_eq!(PacketType::try_from(0x21), Ok(PacketType::Handshake));
        assert_eq!(u8::from(PacketType::Handshake), 0x21);
        assert_eq!(PacketType::try_from(0x41), Ok(PacketType::BindInfo));
        assert_eq!(u8::from(PacketType::BindInfo), 0x41);
        assert_eq!(
            PacketType::try_from(0x50),
            Ok(PacketType::ParameterConfiguration)
        );
        assert_eq!(u8::from(PacketType::ParameterConfiguration), 0x50);
        assert_eq!(PacketType::try_from(0x55), Ok(PacketType::SignalQuality));
        assert_eq!(u8::from(PacketType::SignalQuality), 0x55);
        assert_eq!(
            PacketType::try_from(0x80),
            Ok(PacketType::TelemetrySensorData)
        );
        assert_eq!(u8::from(PacketType::TelemetrySensorData), 0x80);
        assert_eq!(PacketType::try_from(0xCD), Ok(PacketType::ControlData));
        assert_eq!(u8::from(PacketType::ControlData), 0xCD);
        for v in (0x0..=0x20)
            .chain(0x22..=0x40)
            .chain(0x42..=0x49)
            .chain(0x51..=0x54)
            .chain(0x56..=0x79)
            .chain(0x81..=0xCC)
            .chain(0xCE..=0xFF)
        {
            assert_eq!(PacketType::try_from(v), Err(InvalidPacketType(v)));
        }
    }

    #[test]
    fn typed_device_id_conversion() {
        assert_eq!(
            TypedDeviceID::try_from(0x10),
            Ok(TypedDeviceID(DeviceType::RemoteReceiver, UnitID(0x0)))
        );
        assert_eq!(
            u8::from(TypedDeviceID(DeviceType::RemoteReceiver, UnitID(0x0))),
            0x10
        );
        assert_eq!(
            TypedDeviceID::try_from(0x21),
            Ok(TypedDeviceID(DeviceType::Receiver, UnitID(0x1)))
        );
        assert_eq!(
            TypedDeviceID::try_from(0x30),
            Ok(TypedDeviceID(DeviceType::FlightController, UnitID(0x0)))
        );
        assert_eq!(
            TypedDeviceID::try_from(0x4F),
            Ok(TypedDeviceID(DeviceType::ESC, UnitID(0xF)))
        );
        assert_eq!(
            TypedDeviceID::try_from(0x60),
            Ok(TypedDeviceID(
                DeviceType::SRXLServo(ServoPrefix::Low),
                UnitID(0x0)
            ))
        );
        assert_eq!(
            TypedDeviceID::try_from(0x70),
            Ok(TypedDeviceID(
                DeviceType::SRXLServo(ServoPrefix::High),
                UnitID(0x0)
            ))
        );
        for v in (0x0..=0xF)
            .chain([0x20, 0x80])
            .chain(0x50..=0x5F)
            .chain(0x90..=0xFF)
        {
            assert_eq!(TypedDeviceID::try_from(v), Err(InvalidTypedDeviceID(v)));
        }
    }

    #[test]
    fn device_id_conversion() {
        assert_eq!(DeviceID::try_from(0x0), Ok(DeviceID::NotSpecified));
        assert_eq!(u8::from(DeviceID::NotSpecified), 0x0);
        assert_eq!(
            DeviceID::try_from(0x21),
            Ok(DeviceID::Typed(TypedDeviceID(
                DeviceType::Receiver,
                UnitID(0x1)
            )))
        );
        assert_eq!(
            u8::from(DeviceID::Typed(TypedDeviceID(
                DeviceType::Receiver,
                UnitID(0x1)
            ))),
            0x21
        );
        for v in (0x50..=0x5F).chain(0x90..=0xEF) {
            assert_eq!(DeviceID::try_from(v), Ok(DeviceID::Reserved(v)));
            assert_eq!(u8::from(DeviceID::Reserved(v)), v);
        }
        assert_eq!(DeviceID::try_from(0xFF), Ok(DeviceID::Broadcast));
        assert_eq!(u8::from(DeviceID::Broadcast), 0xFF);
        for v in (0x1..=0xF).chain([0x20, 0x80]).chain(0xF0..=0xFE) {
            assert_eq!(DeviceID::try_from(v), Err(InvalidDeviceID(v)));
        }
    }

    #[test]
    fn device_type_conversion() {
        assert_eq!(DeviceType::try_from(0x1), Ok(DeviceType::RemoteReceiver));
        assert_eq!(u8::from(DeviceType::RemoteReceiver), 0x1);
        assert_eq!(DeviceType::try_from(0x2), Ok(DeviceType::Receiver));
        assert_eq!(u8::from(DeviceType::Receiver), 0x2);
        assert_eq!(DeviceType::try_from(0x3), Ok(DeviceType::FlightController));
        assert_eq!(u8::from(DeviceType::FlightController), 0x3);
        assert_eq!(DeviceType::try_from(0x4), Ok(DeviceType::ESC));
        assert_eq!(u8::from(DeviceType::ESC), 0x4);
        assert_eq!(
            DeviceType::try_from(0x6),
            Ok(DeviceType::SRXLServo(ServoPrefix::Low))
        );
        assert_eq!(u8::from(DeviceType::SRXLServo(ServoPrefix::Low)), 0x6);
        assert_eq!(
            DeviceType::try_from(0x7),
            Ok(DeviceType::SRXLServo(ServoPrefix::High))
        );
        assert_eq!(u8::from(DeviceType::SRXLServo(ServoPrefix::High)), 0x7);
        assert_eq!(DeviceType::try_from(0x8), Ok(DeviceType::VTX));
        assert_eq!(u8::from(DeviceType::VTX), 0x8);
        for v in (0x9..=0xFF).chain([0x5]) {
            assert_eq!(DeviceType::try_from(v), Err(InvalidDeviceType(v)));
        }
    }

    #[test]
    fn device_type_methods() {
        for dt in [
            DeviceType::RemoteReceiver,
            DeviceType::FlightController,
            DeviceType::ESC,
            DeviceType::SRXLServo(ServoPrefix::Low),
            DeviceType::SRXLServo(ServoPrefix::High),
        ] {
            assert_eq!(dt.with_id(0x0), Ok(TypedDeviceID(dt, UnitID(0x0))));
            assert_eq!(dt.with_id(0xF), Ok(TypedDeviceID(dt, UnitID(0xF))));
            assert_eq!(dt.with_id(0xCD), Ok(TypedDeviceID(dt, UnitID(0xD))));
        }
        for dt in [DeviceType::Receiver, DeviceType::VTX] {
            assert_eq!(dt.with_id(0x1), Ok(TypedDeviceID(dt, UnitID(0x1))));
            assert_eq!(dt.with_id(0xF), Ok(TypedDeviceID(dt, UnitID(0xF))));
            assert_eq!(dt.with_id(0xCD), Ok(TypedDeviceID(dt, UnitID(0xD))));
        }
        assert_eq!(
            DeviceType::Receiver.with_id(0x0),
            Err(InvalidTypedDeviceID(0x20))
        );
        assert_eq!(
            DeviceType::VTX.with_id(0x0),
            Err(InvalidTypedDeviceID(0x80))
        );
    }

    #[test]
    fn unit_id_conversion() {
        assert_eq!(u8::from(UnitID(0x1)), 0x1);
    }

    #[test]
    fn priority_conversion() {
        for v in 1..=100 {
            assert_eq!(Priority::try_from(v), Ok(Priority(v)));
            assert_eq!(u8::from(Priority(v)), v);
        }
        for v in (101..=u8::MAX).chain([0]) {
            assert_eq!(Priority::try_from(v), Err(InvalidPriority(v)));
        }
    }

    #[test]
    fn baud_supported_conversion() {
        assert_eq!(BaudSupported::try_from(0x0), Ok(BaudSupported::Only115200));
        assert_eq!(u8::from(BaudSupported::Only115200), 0x0);
        assert_eq!(BaudSupported::try_from(0x1), Ok(BaudSupported::Also400000));
        assert_eq!(u8::from(BaudSupported::Also400000), 0x1);
        assert_eq!(BaudSupported::try_from(0x2), Err(InvalidBaudSupported(0x2)));
    }

    #[test]
    fn device_option_conversion() {
        assert_eq!(
            DeviceOptions::try_from(0b001),
            Ok(DeviceOptions::EnableTelemetryOverRF)
        );
        assert_eq!(u8::from(DeviceOptions::EnableTelemetryOverRF), 0b001);
        assert_eq!(
            DeviceOptions::try_from(0b010),
            Ok(DeviceOptions::SendsFullRangeTelemetry)
        );
        assert_eq!(u8::from(DeviceOptions::SendsFullRangeTelemetry), 0b010);
        assert_eq!(
            DeviceOptions::try_from(0b100),
            Ok(DeviceOptions::ForwardProgrammingTarget)
        );
        assert_eq!(u8::from(DeviceOptions::ForwardProgrammingTarget), 0b100);
        assert_eq!(
            DeviceOptions::try_from(0b011),
            Ok(DeviceOptions::EnableTelemetryOverRF | DeviceOptions::SendsFullRangeTelemetry)
        );
        assert_eq!(
            u8::from(DeviceOptions::EnableTelemetryOverRF | DeviceOptions::SendsFullRangeTelemetry),
            0b011
        );
        assert_eq!(
            DeviceOptions::try_from(0b101),
            Ok(DeviceOptions::EnableTelemetryOverRF | DeviceOptions::ForwardProgrammingTarget)
        );
        assert_eq!(
            u8::from(
                DeviceOptions::EnableTelemetryOverRF | DeviceOptions::ForwardProgrammingTarget
            ),
            0b101
        );
        assert_eq!(
            DeviceOptions::try_from(0b110),
            Ok(DeviceOptions::SendsFullRangeTelemetry | DeviceOptions::ForwardProgrammingTarget)
        );
        assert_eq!(
            u8::from(
                DeviceOptions::SendsFullRangeTelemetry | DeviceOptions::ForwardProgrammingTarget
            ),
            0b110
        );
        for v in 0x8..=u8::MAX {
            assert_eq!(DeviceOptions::try_from(v), Err(InvalidDeviceOptions(v)));
        }
    }

    #[test]
    fn bind_request_conversion() {
        assert_eq!(BindRequest::try_from(0xEB), Ok(BindRequest::EnterBindMode));
        assert_eq!(u8::from(BindRequest::EnterBindMode), 0xEB);
        assert_eq!(
            BindRequest::try_from(0xB5),
            Ok(BindRequest::RequestBindStatus)
        );
        assert_eq!(u8::from(BindRequest::RequestBindStatus), 0xB5);
        assert_eq!(
            BindRequest::try_from(0xDB),
            Ok(BindRequest::BoundDataReport)
        );
        assert_eq!(u8::from(BindRequest::BoundDataReport), 0xDB);
        assert_eq!(BindRequest::try_from(0x5B), Ok(BindRequest::SetBindInfo));
        assert_eq!(u8::from(BindRequest::SetBindInfo), 0x5B);
        assert_eq!(BindRequest::try_from(0x0), Err(InvalidBindRequest(0x0)));
    }

    #[test]
    fn bind_status_conversion() {
        assert_eq!(BindStatus::try_from(0x00), Ok(BindStatus::NotBound));
        assert_eq!(u8::from(BindStatus::NotBound), 0x00);
        assert_eq!(
            BindStatus::try_from(0x01),
            Ok(BindStatus::Air(Air::DSM2(DSM2::TwentyTwoMillis)))
        );
        assert_eq!(
            u8::from(BindStatus::Air(Air::DSM2(DSM2::TwentyTwoMillis))),
            0x01
        );
        assert_eq!(
            BindStatus::try_from(0x02),
            Ok(BindStatus::Air(Air::DSM2(DSM2::MC24)))
        );
        assert_eq!(u8::from(BindStatus::Air(Air::DSM2(DSM2::MC24))), 0x02);
        assert_eq!(
            BindStatus::try_from(0x12),
            Ok(BindStatus::Air(Air::DSM2(DSM2::ElevenMillis)))
        );
        assert_eq!(
            u8::from(BindStatus::Air(Air::DSM2(DSM2::ElevenMillis))),
            0x12
        );
        assert_eq!(
            BindStatus::try_from(0xA2),
            Ok(BindStatus::Air(Air::DSMX(DSMX::TwentyTwoMillis)))
        );
        assert_eq!(
            u8::from(BindStatus::Air(Air::DSMX(DSMX::TwentyTwoMillis))),
            0xA2
        );
        assert_eq!(
            BindStatus::try_from(0xB2),
            Ok(BindStatus::Air(Air::DSMX(DSMX::ElevenMillis)))
        );
        assert_eq!(
            u8::from(BindStatus::Air(Air::DSMX(DSMX::ElevenMillis))),
            0xB2
        );
        assert_eq!(
            BindStatus::try_from(0x63),
            Ok(BindStatus::Surface(Surface::DSM2))
        );
        assert_eq!(u8::from(BindStatus::Surface(Surface::DSM2)), 0x63);
        assert_eq!(
            BindStatus::try_from(0xE2),
            Ok(BindStatus::Surface(Surface::DSMR(
                DSMR::ElevenOrTwentyTwoMillis
            )))
        );
        assert_eq!(
            u8::from(BindStatus::Surface(Surface::DSMR(
                DSMR::ElevenOrTwentyTwoMillis
            ))),
            0xE2
        );
        assert_eq!(
            BindStatus::try_from(0xE4),
            Ok(BindStatus::Surface(Surface::DSMR(
                DSMR::FivePointFiveMillis
            )))
        );
        assert_eq!(
            u8::from(BindStatus::Surface(Surface::DSMR(
                DSMR::FivePointFiveMillis
            ))),
            0xE4
        );
        for v in (0x03..=0x11)
            .chain(0x13..=0x62)
            .chain(0x64..=0xA1)
            .chain(0xA3..=0xB1)
            .chain(0xB3..=0xE1)
            .chain([0xE3])
            .chain(0xE5..=u8::MAX)
        {
            assert_eq!(BindStatus::try_from(v), Err(InvalidBindStatus(v)));
        }
    }

    #[test]
    fn bind_option_conversion() {
        assert_eq!(
            BindOptions::try_from(0b001),
            Ok(BindOptions::EnableTelemetryOverRF)
        );
        assert_eq!(u8::from(BindOptions::EnableTelemetryOverRF), 0b001);
        assert_eq!(
            BindOptions::try_from(0b010),
            Ok(BindOptions::AllowBindReplyOverRF)
        );
        assert_eq!(u8::from(BindOptions::AllowBindReplyOverRF), 0b010);
        assert_eq!(
            BindOptions::try_from(0b100),
            Ok(BindOptions::RequestUSPowerLevelForRFTransmits)
        );
        assert_eq!(
            u8::from(BindOptions::RequestUSPowerLevelForRFTransmits),
            0b100
        );
        assert_eq!(
            BindOptions::try_from(0b011),
            Ok(BindOptions::EnableTelemetryOverRF | BindOptions::AllowBindReplyOverRF)
        );
        assert_eq!(
            u8::from(BindOptions::EnableTelemetryOverRF | BindOptions::AllowBindReplyOverRF),
            0b011
        );
        assert_eq!(
            BindOptions::try_from(0b101),
            Ok(BindOptions::EnableTelemetryOverRF | BindOptions::RequestUSPowerLevelForRFTransmits)
        );
        assert_eq!(
            u8::from(
                BindOptions::EnableTelemetryOverRF | BindOptions::RequestUSPowerLevelForRFTransmits
            ),
            0b101
        );
        assert_eq!(
            BindOptions::try_from(0b110),
            Ok(BindOptions::AllowBindReplyOverRF | BindOptions::RequestUSPowerLevelForRFTransmits)
        );
        assert_eq!(
            u8::from(
                BindOptions::AllowBindReplyOverRF | BindOptions::RequestUSPowerLevelForRFTransmits
            ),
            0b110
        );
        for v in 0x8..=u8::MAX {
            assert_eq!(BindOptions::try_from(v), Err(InvalidBindOptions(v)));
        }
    }

    #[test]
    fn parameter_configuration_request_conversion() {
        assert_eq!(
            ParameterConfigurationRequest::try_from(0x50),
            Ok(ParameterConfigurationRequest::QueryParameterValue)
        );
        assert_eq!(
            u8::from(ParameterConfigurationRequest::QueryParameterValue),
            0x50
        );
        assert_eq!(
            ParameterConfigurationRequest::try_from(0x57),
            Ok(ParameterConfigurationRequest::WriteParameterValue)
        );
        assert_eq!(
            u8::from(ParameterConfigurationRequest::WriteParameterValue),
            0x57
        );
        for v in (0x58..=u8::MAX).chain(0x0..=0x49).chain(0x51..=0x56) {
            assert_eq!(
                ParameterConfigurationRequest::try_from(v),
                Err(InvalidParameterConfigurationRequest(v))
            );
        }
    }

    #[test]
    fn param_value_conversion() {
        assert_eq!(
            ParamValue::from(u8::MAX),
            ParamValue(0b00000000000000000000000011111111)
        );
        assert_eq!(
            u8::from(ParamValue(0b00000000000000000000000011111111)),
            u8::MAX
        );
        assert_eq!(
            ParamValue::from(i8::MIN),
            ParamValue(0b11111111111111111111111110000000)
        );
        assert_eq!(
            i8::from(ParamValue(0b11111111111111111111111110000000)),
            i8::MIN
        );
        assert_eq!(
            ParamValue::from(u16::MAX),
            ParamValue(0b00000000000000001111111111111111)
        );
        assert_eq!(
            u16::from(ParamValue(0b00000000000000001111111111111111)),
            u16::MAX
        );
        assert_eq!(
            ParamValue::from(i16::MIN),
            ParamValue(0b11111111111111111000000000000000)
        );
        assert_eq!(
            i16::from(ParamValue(0b11111111111111111000000000000000)),
            i16::MIN
        );
        assert_eq!(
            ParamValue::from(u32::MAX),
            ParamValue(0b11111111111111111111111111111111)
        );
        assert_eq!(
            u32::from(ParamValue(0b11111111111111111111111111111111)),
            u32::MAX
        );
        assert_eq!(
            ParamValue::from(i32::MIN),
            ParamValue(0b10000000000000000000000000000000)
        );
        assert_eq!(
            i32::from(ParamValue(0b10000000000000000000000000000000)),
            i32::MIN
        );
    }

    #[test]
    fn signal_quality_request_conversion() {
        assert_eq!(
            SignalQualityRequest::try_from(0x52),
            Ok(SignalQualityRequest::RequestQualityStatus)
        );
        assert_eq!(u8::from(SignalQualityRequest::RequestQualityStatus), 0x52);
        assert_eq!(
            SignalQualityRequest::try_from(0x53),
            Ok(SignalQualityRequest::QualityStatusReport)
        );
        assert_eq!(u8::from(SignalQualityRequest::QualityStatusReport), 0x53);
        for v in (0x54..=u8::MAX).chain(0x0..=0x51) {
            assert_eq!(
                SignalQualityRequest::try_from(v),
                Err(InvalidSignalQualityRequest(v))
            );
        }
    }

    #[test]
    fn telemetry_packet_conversion() {
        let bytes = [0x42; TELEMETRY_PACKET_LENGTH];
        let packet = TelemetryPacket::new(bytes);
        assert_eq!(<&[u8]>::from(&packet), &bytes[..]);
    }

    #[test]
    fn telemetry_destination_conversion() {
        assert_eq!(
            TelemetryDestination::try_from(0x0),
            Ok(TelemetryDestination::Disabled)
        );
        assert_eq!(u8::from(TelemetryDestination::Disabled), 0x0);
        assert_eq!(
            TelemetryDestination::try_from(0xFF),
            Ok(TelemetryDestination::HandshakeNeeded)
        );
        assert_eq!(u8::from(TelemetryDestination::HandshakeNeeded), 0xFF);
        assert_eq!(
            TelemetryDestination::try_from(0x21),
            Ok(TelemetryDestination::Receiver(TypedDeviceID(
                DeviceType::Receiver,
                UnitID(0x1)
            )))
        );
        assert_eq!(
            u8::from(TelemetryDestination::Receiver(TypedDeviceID(
                DeviceType::Receiver,
                UnitID(0x1)
            ))),
            0x21
        );
        assert_eq!(
            TelemetryDestination::try_from(0x20),
            Err(InvalidTelemetryDestination(InvalidTypedDeviceID(0x20)))
        );
    }

    #[test]
    fn control_data_command_conversion() {
        assert_eq!(
            ControlDataCommand::try_from(0x0),
            Ok(ControlDataCommand::ChannelData)
        );
        assert_eq!(u8::from(ControlDataCommand::ChannelData), 0x0);
        assert_eq!(
            ControlDataCommand::try_from(0x1),
            Ok(ControlDataCommand::FailsafeChannelData)
        );
        assert_eq!(u8::from(ControlDataCommand::FailsafeChannelData), 0x1);
        assert_eq!(
            ControlDataCommand::try_from(0x2),
            Ok(ControlDataCommand::VTXData)
        );
        assert_eq!(u8::from(ControlDataCommand::VTXData), 0x2);
        assert_eq!(
            ControlDataCommand::try_from(0x3),
            Ok(ControlDataCommand::Reserved)
        );
        assert_eq!(u8::from(ControlDataCommand::Reserved), 0x3);
        for v in 0x4..=u8::MAX {
            assert_eq!(
                ControlDataCommand::try_from(v),
                Err(InvalidControlDataCommand(v))
            );
        }
    }

    #[test]
    fn reply_id_conversion() {
        assert_eq!(ReplyID::try_from(0x0), Ok(ReplyID::NoDevice));
        assert_eq!(u8::from(ReplyID::NoDevice), 0x0);
        assert_eq!(ReplyID::try_from(0xFF), Ok(ReplyID::AllDevices));
        assert_eq!(u8::from(ReplyID::AllDevices), 0xFF);
        let device_id = TypedDeviceID(DeviceType::Receiver, UnitID(0x1));
        assert_eq!(ReplyID::try_from(0x21), Ok(ReplyID::SlaveDevice(device_id)));
        assert_eq!(u8::from(ReplyID::SlaveDevice(device_id)), 0x21);
        assert_eq!(
            ReplyID::try_from(0x1),
            Err(InvalidReplyID(InvalidTypedDeviceID(0x1)))
        );
        assert_eq!(ReplyID::from(device_id), ReplyID::SlaveDevice(device_id));
    }

    #[test]
    fn vtx_band_conversion() {
        assert_eq!(VTXBand::try_from(0x0), Ok(VTXBand::Fatshark));
        assert_eq!(u8::from(VTXBand::Fatshark), 0x0);
        assert_eq!(VTXBand::try_from(0x1), Ok(VTXBand::Raceband));
        assert_eq!(u8::from(VTXBand::Raceband), 0x1);
        assert_eq!(VTXBand::try_from(0x2), Ok(VTXBand::E));
        assert_eq!(u8::from(VTXBand::E), 0x2);
        assert_eq!(VTXBand::try_from(0x3), Ok(VTXBand::B));
        assert_eq!(u8::from(VTXBand::B), 0x3);
        assert_eq!(VTXBand::try_from(0x4), Ok(VTXBand::A));
        assert_eq!(u8::from(VTXBand::A), 0x4);
        for v in 0x5..=0x7 {
            assert_eq!(VTXBand::try_from(v), Ok(VTXBand::Reserved(v)));
            assert_eq!(u8::from(VTXBand::Reserved(v)), v);
        }
        for v in 0x8..=u8::MAX {
            assert_eq!(VTXBand::try_from(v), Err(InvalidVTXBand(v)));
        }
    }

    #[test]
    fn vtx_channel_conversion() {
        for v in 0x0..=0x7 {
            assert_eq!(VTXChannel::try_from(v), Ok(VTXChannel(v)));
            assert_eq!(u8::from(VTXChannel(v)), v);
        }
        for v in 0x8..=u8::MAX {
            assert_eq!(VTXChannel::try_from(v), Err(InvalidVTXChannel(v)));
        }
    }

    #[test]
    fn vtx_mode_conversion() {
        assert_eq!(VTXMode::try_from(0x0), Ok(VTXMode::Race));
        assert_eq!(u8::from(VTXMode::Race), 0x0);
        assert_eq!(VTXMode::try_from(0x1), Ok(VTXMode::Pit));
        assert_eq!(u8::from(VTXMode::Pit), 0x1);
        for v in 0x2..=u8::MAX {
            assert_eq!(VTXMode::try_from(v), Err(InvalidVTXMode(v)));
        }
    }

    #[test]
    fn vtx_region_conversion() {
        assert_eq!(VTXRegion::try_from(0x0), Ok(VTXRegion::USA));
        assert_eq!(u8::from(VTXRegion::USA), 0x0);
        assert_eq!(VTXRegion::try_from(0x1), Ok(VTXRegion::EU));
        assert_eq!(u8::from(VTXRegion::EU), 0x1);
        for v in 0x2..=u8::MAX {
            assert_eq!(VTXRegion::try_from(v), Err(InvalidVTXRegion(v)));
        }
    }

    #[test]
    fn vtx_power_conversion() {
        assert_eq!(VTXPower::try_from((0x0, 0xFFFF)), Ok(VTXPower::Off));
        assert_eq!(<(u8, u16)>::from(VTXPower::Off), (0x0, 0xFFFF));
        assert_eq!(
            VTXPower::try_from((0x1, 0xFFFF)),
            Ok(VTXPower::Milliwatts1_14)
        );
        assert_eq!(<(u8, u16)>::from(VTXPower::Milliwatts1_14), (0x1, 0xFFFF));
        assert_eq!(
            VTXPower::try_from((0x2, 0xFFFF)),
            Ok(VTXPower::Milliwatts15_25)
        );
        assert_eq!(<(u8, u16)>::from(VTXPower::Milliwatts15_25), (0x2, 0xFFFF));
        assert_eq!(
            VTXPower::try_from((0x3, 0xFFFF)),
            Ok(VTXPower::Milliwatts26_99)
        );
        assert_eq!(<(u8, u16)>::from(VTXPower::Milliwatts26_99), (0x3, 0xFFFF));
        assert_eq!(
            VTXPower::try_from((0x4, 0xFFFF)),
            Ok(VTXPower::Milliwatts100_299)
        );
        assert_eq!(
            <(u8, u16)>::from(VTXPower::Milliwatts100_299),
            (0x4, 0xFFFF)
        );
        assert_eq!(
            VTXPower::try_from((0x5, 0xFFFF)),
            Ok(VTXPower::Milliwatts300_600)
        );
        assert_eq!(
            <(u8, u16)>::from(VTXPower::Milliwatts300_600),
            (0x5, 0xFFFF)
        );
        assert_eq!(
            VTXPower::try_from((0x6, 0xFFFF)),
            Ok(VTXPower::Milliwatts601Plus)
        );
        assert_eq!(
            <(u8, u16)>::from(VTXPower::Milliwatts601Plus),
            (0x6, 0xFFFF)
        );
        assert_eq!(
            VTXPower::try_from((0x7, 0xFFFF)),
            Ok(VTXPower::ManualControl)
        );
        assert_eq!(<(u8, u16)>::from(VTXPower::ManualControl), (0x7, 0xFFFF));
        assert_eq!(VTXPower::try_from((0x0, 0xABC)), Ok(VTXPower::Off));
        for v in 0x0..u16::MAX {
            assert_eq!(
                VTXPower::try_from((0xFF, v)),
                Ok(VTXPower::DecimalMilliwatts(v))
            );
            assert_eq!(<(u8, u16)>::from(VTXPower::DecimalMilliwatts(v)), (0xFF, v));
        }
        assert_eq!(VTXPower::try_from((0xFF, 0xFFFF)), Ok(VTXPower::NoChange));
        assert_eq!(<(u8, u16)>::from(VTXPower::NoChange), (0xFF, 0xFFFF));
        for v in 0x8..u8::MAX {
            assert_eq!(
                VTXPower::try_from((v, 0xFFFF)),
                Err(InvalidVTXPower(v, 0xFFFF))
            );
        }
    }

    #[test]
    fn rssi_conversion() {
        for v in 1..=i8::MAX {
            assert_eq!(RSSI::from(v), RSSI::RangeRemaining(RangeRemaining(v)));
        }
        for v in i8::MIN..=-1 {
            assert_eq!(RSSI::from(v), RSSI::DecibelMilliwatts(DecibelMilliwatts(v)));
        }
        assert_eq!(RSSI::from(0), RSSI::NoData);
    }

    #[test]
    fn frame_losses_conversion() {
        assert_eq!(FrameLosses::default(), FrameLosses(0));
    }

    #[test]
    fn channel_value_conversion() {
        for v in [0, 0x8000, 0xFFFC] {
            assert_eq!(ChannelValue::try_from(v), Ok(ChannelValue(v)));
            assert_eq!(u16::from(ChannelValue(v)), v);
        }
        for v in [
            0x1, 0x2, 0x3, 0x8001, 0x8002, 0x8003, 0xFFFD, 0xFFFE, 0xFFFF,
        ] {
            assert_eq!(ChannelValue::try_from(v), Err(InvalidChannelValue(v)));
        }
    }

    #[test]
    fn channel_values_conversion() {
        let mask = 0b1;
        let values = Vec::from_iter([ChannelValue(0x8000)]);
        assert_eq!(
            ChannelValues::try_from((mask, values.clone())),
            Ok(ChannelValues { mask, values })
        );
        assert_eq!(
            ChannelValues::try_from((0b0, Vec::new())),
            Ok(ChannelValues {
                mask: 0,
                values: Vec::new()
            })
        );
        assert_eq!(
            ChannelValues::try_from((0b1, Vec::new())),
            Err(InvalidChannelValues { bits: 1, values: 0 })
        );
    }

    #[test]
    fn rf_link_state_conversion() {
        let mask = 0b1;
        let values = Vec::from_iter([ChannelValue(0x8000)]);
        assert_eq!(
            RFLinkState::try_from((mask, values.clone())),
            Ok(RFLinkState::Nominal(ChannelValues { mask, values }))
        );
        assert_eq!(
            RFLinkState::try_from((0b0, Vec::new())),
            Ok(RFLinkState::Fade)
        );
        assert_eq!(
            RFLinkState::try_from((0b1, Vec::new())),
            Err(InvalidChannelValues { bits: 1, values: 0 })
        );
    }

    #[test]
    fn holds_conversion() {
        assert_eq!(Holds::default(), Holds(0));
    }
}
