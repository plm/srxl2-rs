use super::*;

use core::ops::Index;
use core::ops::{Add, BitAnd, BitAndAssign, RangeFull};

impl BitAnd for BaudSupported {
    type Output = Self;

    fn bitand(self, other: Self) -> Self {
        match self {
            Self::Only115200 => self,
            Self::Also400000 => other,
        }
    }
}

impl BitAndAssign for BaudSupported {
    fn bitand_assign(&mut self, other: Self) {
        *self = *self & other;
    }
}

impl Index<RangeFull> for TelemetryPacket {
    type Output = [u8];

    fn index(&self, range: RangeFull) -> &Self::Output {
        &self.0[range]
    }
}

impl Add for FrameLosses {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl Add for Holds {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn baud_supported_operators() {
        for baud in [BaudSupported::Only115200, BaudSupported::Also400000] {
            assert_eq!(BaudSupported::Only115200 & baud, BaudSupported::Only115200);
            assert_eq!(baud & BaudSupported::Only115200, BaudSupported::Only115200);
            let mut other_baud = baud;
            other_baud &= BaudSupported::Only115200;
            assert_eq!(other_baud, BaudSupported::Only115200);
            let mut baud_115 = BaudSupported::Only115200;
            baud_115 &= baud;
            assert_eq!(baud_115, BaudSupported::Only115200);
        }
        assert_eq!(
            BaudSupported::Also400000 & BaudSupported::Also400000,
            BaudSupported::Also400000
        );
        let mut baud_400 = BaudSupported::Also400000;
        baud_400 &= BaudSupported::Also400000;
        assert_eq!(baud_400, BaudSupported::Also400000);
    }

    #[test]
    fn telemetry_packet_operators() {
        let bytes = [
            0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF,
        ];
        let packet = TelemetryPacket::new(bytes);
        assert_eq!(&packet[..], &bytes[..]);
    }

    #[test]
    fn frame_losses_operators() {
        assert_eq!(FrameLosses(1) + FrameLosses(2), FrameLosses(3));
    }

    #[test]
    fn holds_operators() {
        assert_eq!(Holds(1) + Holds(2), Holds(3));
    }
}
