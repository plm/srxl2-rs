use super::protocol::*;
use core::convert::Into;
use core::mem;
use crc::{Crc, CRC_16_XMODEM};
use paste::paste;

#[derive(Debug, PartialEq, Eq)]
pub struct WriteBuffer {
    buffer: [u8; MAX_PACKET_LENGTH],
    offset: usize,
}

macro_rules! put_impl {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        fn [<put_ $t>](&mut self, value: impl Into<$t>) {
            let index = self.offset;
            self.buffer[index] = value.into() as u8;
            self.offset = index + mem::size_of::<$t>();
        }
        }
        )+
    }
}

macro_rules! put_impl_le {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        fn [<put_ $t>](&mut self, value: impl Into<$t>) {
            let start = self.offset;
            let end = start + mem::size_of::<$t>();
            self.buffer[start..end].copy_from_slice(&value.into().to_le_bytes());
            self.offset = end;
        }
        }
        )+
    }
}

impl WriteBuffer {
    pub const fn new() -> Self {
        Self {
            buffer: [0x0; MAX_PACKET_LENGTH],
            offset: 0,
        }
    }

    put_impl! { i8 u8 }

    put_impl_le! { u16 u32 u64 }

    fn put_slice(&mut self, slice: &[u8]) {
        let start = self.offset;
        let end = start + slice.len();
        self.buffer[start..end].copy_from_slice(slice);
        self.offset = end;
    }

    fn put_handshake(&mut self, handshake: &Handshake) {
        self.put_u8(handshake.source);
        self.put_u8(handshake.destination);
        self.put_u8(handshake.priority);
        self.put_u8(handshake.baud_supported);
        self.put_u8(handshake.options);
        self.put_u32(handshake.uid);
    }

    fn put_bind_info(&mut self, bind_info: &BindInfo) {
        self.put_u8(bind_info.request);
        self.put_u8(bind_info.device);
        self.put_u8(bind_info.status);
        self.put_u8(bind_info.options);
        self.put_u64(bind_info.guid);
        self.put_u32(bind_info.uid);
    }

    fn put_parameter_configuration(&mut self, param_config: &ParameterConfiguration) {
        self.put_u8(param_config.request());
        self.put_u8(param_config.destination);
        self.put_u32(param_config.id());
        self.put_u32(param_config.value().map_or(u32::MAX, Into::into));
    }

    fn put_signal_quality(&mut self, signal_quality: &SignalQuality) {
        self.put_u8(signal_quality.request);
        self.put_i8(signal_quality.antenna_a);
        self.put_i8(signal_quality.antenna_b);
        self.put_i8(signal_quality.antenna_l);
        self.put_i8(signal_quality.antenna_r);
    }

    fn put_telemetry_sensor_data(&mut self, sensor_data: &TelemetrySensorData) {
        self.put_u8(sensor_data.destination);
        self.put_slice(&sensor_data.packet[..]);
    }

    fn put_channel_values(&mut self, channel_values: &ChannelValues) {
        self.put_u32(channel_values.mask());
        for &value in channel_values {
            self.put_u16(value);
        }
    }

    fn put_channel_data(&mut self, channel_data: &ChannelData) {
        self.put_i8(channel_data.rssi);
        self.put_u16(channel_data.frame_losses);
        match &channel_data.link_state {
            RFLinkState::Nominal(values) => {
                self.put_channel_values(values);
            }
            RFLinkState::Fade => {
                self.put_u32(0b0_u32);
            }
        }
    }

    fn put_failsafe_channel_data(&mut self, channel_data: &FailsafeChannelData) {
        self.put_i8(channel_data.rssi_min);
        self.put_u16(channel_data.holds);
        self.put_channel_values(channel_data.values());
    }

    fn put_vtx_data(&mut self, vtx_data: &VTXData) {
        self.put_u8(vtx_data.band);
        self.put_u8(vtx_data.channel);
        self.put_u8(vtx_data.mode);
        let (power, decimal) = vtx_data.power.into();
        self.put_u8(power);
        self.put_u16(decimal);
        self.put_u8(vtx_data.region);
    }

    fn put_control_data(&mut self, control_data: &ControlData) {
        self.put_u8(control_data.command());
        self.put_u8(control_data.reply_id);
        match &control_data.payload {
            ControlDataPayload::ChannelData(cd) => self.put_channel_data(cd),
            ControlDataPayload::FailsafeChannelData(fcd) => self.put_failsafe_channel_data(fcd),
            ControlDataPayload::VTXData(vtx) => self.put_vtx_data(vtx),
            ControlDataPayload::Reserved(bytes) => self.put_slice(bytes),
        };
    }

    pub fn write(&mut self, packet: &PacketPayload) -> &[u8] {
        self.buffer[0] = Protocol::SRXL2;
        self.buffer[1] = packet.packet_type().into();
        self.offset = 3; // First byte of payload
        match packet {
            PacketPayload::Handshake(hs) => self.put_handshake(hs),
            PacketPayload::BindInfo(bi) => self.put_bind_info(bi),
            PacketPayload::ParameterConfiguration(pc) => self.put_parameter_configuration(pc),
            PacketPayload::SignalQuality(sq) => self.put_signal_quality(sq),
            PacketPayload::TelemetrySensorData(tsd) => self.put_telemetry_sensor_data(tsd),
            PacketPayload::ControlData(cd) => self.put_control_data(cd),
        };
        let data_end = self.offset;
        let crc_end = data_end + mem::size_of::<u16>();
        self.buffer[2] = crc_end as u8;
        let crc = Crc::<u16>::new(&CRC_16_XMODEM).checksum(&self.buffer[..data_end]);
        self.buffer[data_end..crc_end].copy_from_slice(&crc.to_be_bytes());
        self.offset = 0;
        &self.buffer[..crc_end]
    }
}

impl Default for WriteBuffer {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn write_buffer_methods() {
        let mut buf = WriteBuffer::new();
        assert_eq!(buf, WriteBuffer::default());
        assert_eq!(buf.buffer.len(), MAX_PACKET_LENGTH);
        assert_eq!(buf.offset, 0);
        buf.put_u8(0x42);
        assert_eq!(buf.offset, 1);
        assert_eq!(&buf.buffer[..1], &[0x42]);
        buf.put_i8(-20);
        assert_eq!(buf.offset, 2);
        buf.put_u16(0x1234_u16);
        assert_eq!(buf.offset, 4);
        assert_eq!(&buf.buffer[..4], &[0x42, 0xEC, 0x34, 0x12]);
        buf.put_u32(0x12345678_u32);
        assert_eq!(buf.offset, 8);
        assert_eq!(&buf.buffer[4..8], &[0x78, 0x56, 0x34, 0x12]);
        buf.put_u64(0x0102030405060708_u64);
        assert_eq!(buf.offset, 16);
        assert_eq!(
            &buf.buffer[8..16],
            &[0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01]
        );
        let payload = PacketPayload::SignalQuality(SignalQuality::new(
            SignalQualityRequest::RequestQualityStatus,
            RSSI::from(1),
            RSSI::from(-1),
            RSSI::NoData,
            RSSI::NoData,
        ));
        assert_eq!(
            buf.write(&payload),
            &[0xA6, 0x55, 0x0A, 0x52, 0x01, 0xFF, 0x00, 0x00, 0xFB, 0x76]
        );
        assert_eq!(buf.offset, 0);
    }

    #[test]
    #[should_panic(expected = "index out of bounds: the len is 80 but the index is 80")]
    fn write_buffer_panic_when_full() {
        let mut buf = WriteBuffer::new();
        buf.put_slice(&[0x1; MAX_PACKET_LENGTH]);
        buf.put_u8(0xFF);
        unreachable!()
    }
}
