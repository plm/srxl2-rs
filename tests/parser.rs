use srxl2::{parser::*, protocol::*};

#[test]
fn read_handshake() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0x21, 0x0E, 0x21, 0x40, 0x0A, 0x00, 0x03, 0x78, 0x56, 0x34, 0x12, 0xF3, 0x91,
    ]);
    if let Ok(PacketPayload::Handshake(handshake)) = buffer.read() {
        assert_eq!(handshake.source, TypedDeviceID::try_from(0x21).unwrap());
        assert_eq!(handshake.destination, DeviceID::try_from(0x40).unwrap());
        assert_eq!(handshake.priority, Priority::try_from(0x0A).unwrap());
        assert_eq!(handshake.baud_supported, BaudSupported::Only115200);
        assert_eq!(
            handshake.options,
            DeviceOptions::EnableTelemetryOverRF | DeviceOptions::SendsFullRangeTelemetry
        );
        assert_eq!(handshake.uid, UniqueID::from(0x12_34_56_78));
    } else {
        unreachable!();
    }
}

#[test]
fn read_bind_info() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0x41, 0x15, 0xEB, 0xFF, 0x00, 0x03, 0x12, 0x34, 0x56, 0x78, 0x90, 0x12, 0x34, 0x56,
        0x11, 0x22, 0x33, 0x44, 0x65, 0x68,
    ]);
    if let Ok(PacketPayload::BindInfo(bind_info)) = buffer.read() {
        assert_eq!(bind_info.request, BindRequest::EnterBindMode);
        assert_eq!(bind_info.device, DeviceID::Broadcast);
        assert_eq!(bind_info.status, BindStatus::NotBound);
        assert_eq!(
            bind_info.options,
            BindOptions::EnableTelemetryOverRF | BindOptions::AllowBindReplyOverRF
        );
        assert_eq!(bind_info.guid, GUID::from(0x56_34_12_90_78_56_34_12));
        assert_eq!(bind_info.uid, UniqueID::from(0x44_33_22_11));
    } else {
        unreachable!();
    }
}

#[test]
fn read_parameter_configuration() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0x50, 0x0F, 0x50, 0x40, 0x78, 0x56, 0x34, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0xDA, 0x8A,
    ]);
    if let Ok(PacketPayload::ParameterConfiguration(param_config)) = buffer.read() {
        assert_eq!(
            param_config.request(),
            ParameterConfigurationRequest::QueryParameterValue
        );
        assert_eq!(
            param_config.destination,
            TypedDeviceID::try_from(0x40).unwrap()
        );
        assert_eq!(param_config.id(), ParamID::from(0x12_34_56_78));
        assert_eq!(param_config.value(), None);
    } else {
        unreachable!();
    }
}

#[test]
fn read_signal_quality() {
    let mut buffer =
        ReadBuffer::from_bytes([0xA6, 0x55, 0x0A, 0x52, 0x01, 0xFF, 0x00, 0x00, 0xFB, 0x76]);
    if let Ok(PacketPayload::SignalQuality(signal_quality)) = buffer.read() {
        assert_eq!(
            signal_quality.request,
            SignalQualityRequest::RequestQualityStatus
        );
        assert_eq!(signal_quality.antenna_a, RSSI::from(1));
        assert_eq!(signal_quality.antenna_b, RSSI::from(-1));
        assert_eq!(signal_quality.antenna_l, RSSI::NoData);
        assert_eq!(signal_quality.antenna_r, RSSI::NoData);
    } else {
        unreachable!();
    }
}

#[test]
fn read_telemetry_sensor_data() {
    let bytes = [
        0xA6, 0x80, 0x16, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0B, 0x6E,
    ];
    let mut buffer = ReadBuffer::from_bytes(bytes);
    if let Ok(PacketPayload::TelemetrySensorData(telemetry_sensor_data)) = buffer.read() {
        assert_eq!(
            telemetry_sensor_data.destination,
            TelemetryDestination::Disabled
        );
        assert_eq!(&telemetry_sensor_data.packet[..], &bytes[4..20]);
    } else {
        unreachable!();
    }
}

#[test]
fn read_channel_data() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0xCD, 0x1C, 0x00, 0x30, 0x58, 0x0B, 0x00, 0x37, 0x06, 0x00, 0x00, 0xA0, 0x2A, 0x00,
        0x80, 0x04, 0x80, 0xFC, 0x7F, 0x54, 0xD5, 0xA0, 0x2A, 0xA0, 0x2A, 0xD3, 0x5D,
    ]);
    if let Ok(PacketPayload::ControlData(control_data)) = buffer.read() {
        assert_eq!(
            control_data.reply_id,
            ReplyID::SlaveDevice(TypedDeviceID::try_from(0x30).unwrap())
        );
        if let ControlDataPayload::ChannelData(channel_data) = control_data.payload {
            assert_eq!(channel_data.rssi, RSSI::from(88));
            assert_eq!(channel_data.frame_losses, FrameLosses::from(11));
            assert_eq!(channel_data.get(1), ChannelValue::try_from(10912).ok());
            assert_eq!(channel_data.get(2), ChannelValue::try_from(32768).ok());
            assert_eq!(channel_data.get(3), ChannelValue::try_from(32772).ok());
            assert_eq!(channel_data.get(5), ChannelValue::try_from(32764).ok());
            assert_eq!(channel_data.get(6), ChannelValue::try_from(54612).ok());
            assert_eq!(channel_data.get(10), ChannelValue::try_from(10912).ok());
            assert_eq!(channel_data.get(11), ChannelValue::try_from(10912).ok());
            for c in (12..=32).chain([4, 7, 8, 9]) {
                assert_eq!(channel_data.get(c), None);
            }
        } else {
            unreachable!();
        }
    } else {
        unreachable!();
    }
}

#[test]
fn read_failsafe_channel_data() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0xCD, 0x12, 0x01, 0x30, 0x00, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00,
        0x80, 0x6E, 0xBF,
    ]);
    if let Ok(PacketPayload::ControlData(control_data)) = buffer.read() {
        assert_eq!(
            control_data.reply_id,
            ReplyID::SlaveDevice(TypedDeviceID::try_from(0x30).unwrap())
        );
        if let ControlDataPayload::FailsafeChannelData(failsafe_data) = control_data.payload {
            assert_eq!(failsafe_data.rssi_min, RSSI::NoData);
            assert_eq!(failsafe_data.holds, Holds::from(5));
            assert_eq!(failsafe_data.get(1), ChannelValue::try_from(32768).ok());
            assert_eq!(failsafe_data.get(2), ChannelValue::try_from(32768).ok());
            for c in 3..=32 {
                assert_eq!(failsafe_data.get(c), None);
            }
        } else {
            unreachable!();
        }
    } else {
        unreachable!();
    }
}

#[test]
fn read_vtx_data() {
    let mut buffer = ReadBuffer::from_bytes([
        0xA6, 0xCD, 0x0E, 0x02, 0x81, 0x01, 0x03, 0x00, 0x02, 0xFF, 0xFF, 0x00, 0x3B, 0x74,
    ]);
    if let Ok(PacketPayload::ControlData(control_data)) = buffer.read() {
        assert_eq!(
            control_data.reply_id,
            ReplyID::SlaveDevice(TypedDeviceID::try_from(0x81).unwrap())
        );
        if let ControlDataPayload::VTXData(vtx_data) = control_data.payload {
            assert_eq!(vtx_data.band, VTXBand::Raceband);
            assert_eq!(vtx_data.channel, VTXChannel::try_from(3).unwrap());
            assert_eq!(vtx_data.mode, VTXMode::Race);
            assert_eq!(vtx_data.power, VTXPower::Milliwatts15_25);
            assert_eq!(vtx_data.region, VTXRegion::USA);
        } else {
            unreachable!();
        }
    } else {
        unreachable!();
    }
}
