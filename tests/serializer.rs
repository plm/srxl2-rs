use heapless::Vec;
use srxl2::{protocol::*, serializer::*};

#[test]
fn write_handshake() {
    let handshake = Handshake::new(
        DeviceType::ESC.with_id(0x2).unwrap(),
        DeviceID::NotSpecified,
        Priority::try_from(10).unwrap(),
        BaudSupported::Only115200,
        DeviceOptions::empty(),
        UniqueID::from(0x12345678),
    );
    assert_eq!(
        WriteBuffer::new().write(&handshake.into()),
        &[0xA6, 0x21, 0x0E, 0x42, 0x00, 0x0A, 0x00, 0x00, 0x78, 0x56, 0x34, 0x12, 0x3E, 0xF2]
    );
}

#[test]
fn write_bind_info() {
    let bind_info = BindInfo::new(
        BindRequest::EnterBindMode,
        DeviceID::Broadcast,
        BindStatus::NotBound,
        BindOptions::EnableTelemetryOverRF | BindOptions::AllowBindReplyOverRF,
        GUID::from(0x90_78_56_34_12),
        UniqueID::from(0x44_33_22_11),
    );
    assert_eq!(
        WriteBuffer::new().write(&bind_info.into()),
        &[
            0xA6, 0x41, 0x15, 0xEB, 0xFF, 0x00, 0x03, 0x12, 0x34, 0x56, 0x78, 0x90, 0x00, 0x00,
            0x00, 0x11, 0x22, 0x33, 0x44, 0xC3, 0x6A
        ]
    );
}

#[test]
fn write_parameter_configuration() {
    let parameter_configuration = ParameterConfiguration::new(
        DeviceType::ESC.default_id(),
        ParameterConfigurationPayload::QueryParameterValue(ParamID::from(0x12_34_56_78)),
    );
    assert_eq!(
        WriteBuffer::new().write(&parameter_configuration.into()),
        &[
            0xA6, 0x50, 0x0F, 0x50, 0x40, 0x78, 0x56, 0x34, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0xDA,
            0x8A
        ]
    );
}

#[test]
fn write_signal_quality() {
    let signal_quality = SignalQuality::new(
        SignalQualityRequest::RequestQualityStatus,
        RSSI::from(1),
        RSSI::from(-1),
        RSSI::NoData,
        RSSI::NoData,
    );
    assert_eq!(
        WriteBuffer::new().write(&signal_quality.into()),
        &[0xA6, 0x55, 0x0A, 0x52, 0x01, 0xFF, 0x00, 0x00, 0xFB, 0x76]
    );
}

#[test]
fn write_telemetry_sensor_data() {
    let telemetry_data =
        TelemetrySensorData::new(TelemetryDestination::Disabled, TelemetryPacket::default());
    assert_eq!(
        WriteBuffer::new().write(&telemetry_data.into()),
        &[
            0xA6, 0x80, 0x16, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0B, 0x6E
        ]
    );
}

#[test]
fn write_channel_data() {
    let channel_data = ChannelData::new(
        RSSI::from(88),
        FrameLosses::from(11),
        RFLinkState::try_from((0b1, Vec::from_iter(ChannelValue::try_from(32768).ok()))).unwrap(),
    );
    let control_data = ControlData::new(
        ReplyID::try_from(0x30).unwrap(),
        ControlDataPayload::ChannelData(channel_data),
    );
    assert_eq!(
        WriteBuffer::new().write(&control_data.into()),
        &[
            0xA6, 0xCD, 0x10, 0x00, 0x30, 0x58, 0x0B, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80,
            0xD8, 0xA6
        ]
    );
}

#[test]
fn write_failsafe_channel_data() {
    let failsafe_data = FailsafeChannelData::new(
        RSSI::from(88),
        Holds::from(7),
        ChannelValues::try_from((0b1, Vec::from_iter(ChannelValue::try_from(32768).ok()))).unwrap(),
    );
    let control_data = ControlData::new(
        ReplyID::try_from(0x30).unwrap(),
        ControlDataPayload::FailsafeChannelData(failsafe_data),
    );
    assert_eq!(
        WriteBuffer::new().write(&control_data.into()),
        &[
            0xA6, 0xCD, 0x10, 0x01, 0x30, 0x58, 0x07, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80,
            0x11, 0x58
        ]
    )
}

#[test]
fn write_vtx_data() {
    let vtx_data = VTXData::new(
        VTXBand::Raceband,
        VTXChannel::try_from(3).unwrap(),
        VTXMode::Race,
        VTXPower::Milliwatts15_25,
        VTXRegion::USA,
    );
    let control_data = ControlData::new(
        ReplyID::try_from(0x81).unwrap(),
        ControlDataPayload::VTXData(vtx_data),
    );
    assert_eq!(
        WriteBuffer::new().write(&control_data.into()),
        &[0xA6, 0xCD, 0x0E, 0x02, 0x81, 0x01, 0x03, 0x00, 0x02, 0xFF, 0xFF, 0x00, 0x3B, 0x74]
    )
}
